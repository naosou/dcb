#ifndef _DCB_PAPI_H
#define _DCB_PAPI_H     1

#include "pwr.h"

struct info_power_t{
  int num_objs;
  int num_objs_cpu;
  int num_objs_mem;
  PWR_Cntxt cntxt;
  PWR_Obj *objs;
  PWR_Grp grp;
  PWR_Status stat;
  double energy_start, energy_end, *energy_sum;
  PWR_Time ts_start, ts_end;
  PWR_TimePeriod *period;
};

// extern struct info_power_t info_power;

void init_power_api(int const me_proc_intra);
void free_power_api(int const me_proc_intra);
void start_power_api(int const me_proc_intra);
void stop_power_api(int const me_proc_intra);
void get_result_power_api(int const me_proc_intra, float *power_cpu, float *power_drm, float *power_node);

#endif
