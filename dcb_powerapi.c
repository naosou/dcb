#include <stdlib.h>
#include <stdio.h>

#include <mpi.h>

#include "dcb_intra.h"
#include "dcb_papi.h"

struct info_power_t info_power = {
				  .num_objs     = 13,
				  .num_objs_cpu = 8,
				  .num_objs_mem = 4,
};

void init_power_api(int const me_proc_intra)
{
  int rc, rca[info_power.num_objs];

  if(info_bind.flag_mes_power){
    if(me_proc_intra == 0){

      //Init context
      rc = PWR_CntxtInit(PWR_CNTXT_FX1000, PWR_ROLE_APP, "app", &info_power.cntxt);
      if (rc != PWR_RET_SUCCESS) {
	printf("CntxtInit Failed\n");
	MPI_Abort(MPI_COMM_WORLD, -1);
      }
      //Create group
      rc = PWR_GrpCreate(info_power.cntxt, &info_power.grp);
      if (rc != PWR_RET_SUCCESS) {
	printf("GrpCreate Failed\n");
	MPI_Abort(MPI_COMM_WORLD, -1);
      }

      //Malloc for objects
      if((info_power.objs = (PWR_Obj *)malloc(sizeof(PWR_Obj) * info_power.num_objs)) == NULL){
	fprintf(stderr, "DCB : PowerAPI : Memory allocation failed\n");
	MPI_Abort(MPI_COMM_WORLD, -1);
      }
      
      rca[0]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node",		      &info_power.objs[0]);
      rca[1]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg0.cores",   &info_power.objs[1]);
      rca[2]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg1.cores",   &info_power.objs[2]);
      rca[3]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg2.cores",   &info_power.objs[3]);
      rca[4]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg3.cores",   &info_power.objs[4]);
      rca[5]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg0.l2cache", &info_power.objs[5]);
      rca[6]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg1.l2cache", &info_power.objs[6]);
      rca[7]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg2.l2cache", &info_power.objs[7]);
      rca[8]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.cpu.cmg3.l2cache", &info_power.objs[8]);
      rca[9]  = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.mem0",	      &info_power.objs[9]);
      rca[10] = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.mem1",	      &info_power.objs[10]);
      rca[11] = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.mem2",	      &info_power.objs[11]);
      rca[12] = PWR_CntxtGetObjByName(info_power.cntxt, "plat.node.mem3",	      &info_power.objs[12]);
      for(int i = 0; i < info_power.num_objs; i++){
	if(rca[i] != PWR_RET_SUCCESS){
	  printf("DCB : Error with calling PWR_CntxtGetObjByName, ID = %d, rc = %d\n", i, rca[i]);
	  MPI_Abort(MPI_COMM_WORLD, -1);
	}
      }

      rca[0]  = PWR_GrpAddObj(info_power.grp, info_power.objs[0]);
      rca[1]  = PWR_GrpAddObj(info_power.grp, info_power.objs[1]);
      rca[2]  = PWR_GrpAddObj(info_power.grp, info_power.objs[2]);
      rca[3]  = PWR_GrpAddObj(info_power.grp, info_power.objs[3]);
      rca[4]  = PWR_GrpAddObj(info_power.grp, info_power.objs[4]);
      rca[5]  = PWR_GrpAddObj(info_power.grp, info_power.objs[5]);
      rca[6]  = PWR_GrpAddObj(info_power.grp, info_power.objs[6]);
      rca[7]  = PWR_GrpAddObj(info_power.grp, info_power.objs[7]);
      rca[8]  = PWR_GrpAddObj(info_power.grp, info_power.objs[8]);
      rca[9]  = PWR_GrpAddObj(info_power.grp, info_power.objs[9]);
      rca[10] = PWR_GrpAddObj(info_power.grp, info_power.objs[10]);
      rca[11] = PWR_GrpAddObj(info_power.grp, info_power.objs[11]);
      rca[12] = PWR_GrpAddObj(info_power.grp, info_power.objs[12]);
      for(int i = 0; i < info_power.num_objs; i++){
	if(rca[i] != PWR_RET_SUCCESS){
	  printf("DCB : Error with calling PWR_GrpAddObj, ID = %d, rc = %d\n", i, rca[i]);
	  MPI_Abort(MPI_COMM_WORLD, -1);
	}
      }

      rc = PWR_GrpCreateStat(info_power.grp, PWR_ATTR_POWER, PWR_ATTR_STAT_SUM, &info_power.stat);
      if (rc != PWR_RET_SUCCESS) {
	printf("GrpCreateStat Failed\n");
	MPI_Abort(MPI_COMM_WORLD, -1);
      }

      //Malloc for values
      if((info_power.energy_sum = (double *)malloc(sizeof(double) * info_power.num_objs)) == NULL){
	fprintf(stderr, "DCB : PowerAPI : Memory allocation failed\n");
	MPI_Abort(MPI_COMM_WORLD, -1);
      }
      if((info_power.period = (PWR_TimePeriod *)malloc(sizeof(PWR_TimePeriod) * info_power.num_objs)) == NULL){
	fprintf(stderr, "DCB : PowerAPI : Memory allocation failed\n");
	MPI_Abort(MPI_COMM_WORLD, -1);
      }
    }
  }
  
  return;
}

void free_power_api(int const me_proc_intra)
{
  if(info_bind.flag_mes_power){
    if(me_proc_intra == 0){
      PWR_StatDestroy(info_power.stat);
      PWR_CntxtDestroy(info_power.cntxt);
    }
  }
  return;
}

void start_power_api(int const me_proc_intra)
{
  int rc;
  
  if(info_bind.flag_mes_power){
    if(me_proc_intra == 0){
      rc = PWR_ObjAttrGetValue(info_power.objs[0], PWR_ATTR_MEASURED_ENERGY, &info_power.energy_start, &info_power.ts_start);
      if (rc != PWR_RET_SUCCESS) {
	printf("ObjAttrGetValue1 Failed (rc = %d)\n", rc);
	MPI_Abort(MPI_COMM_WORLD, -1);
      }
      rc = PWR_StatStart(info_power.stat);
    }
  }
  
  return ;
}

void stop_power_api(int const me_proc_intra)
{
  int rc;
  
  if(info_bind.flag_mes_power){
    if(me_proc_intra == 0){
      rc = PWR_StatStop(info_power.stat);
      
      rc = PWR_ObjAttrGetValue(info_power.objs[0], PWR_ATTR_MEASURED_ENERGY, &info_power.energy_end, &info_power.ts_end);
      if (rc != PWR_RET_SUCCESS) {
	printf("ObjAttrGetValue1 Failed (rc = %d)\n", rc);
	MPI_Abort(MPI_COMM_WORLD, -1);
      }
    }
  }

  return ;
}

void get_result_power_api(int const me_proc_intra, float *power_cpu, float *power_drm, float *power_node)
{
  int rc;
  
  if(info_bind.flag_mes_power){
    if(me_proc_intra == 0){
      // Get result of statistics
      rc = PWR_StatGetValues(info_power.stat, info_power.energy_sum, info_power.period);
      if (rc != PWR_RET_SUCCESS) {
	printf("StatGetValue Failed (rc = %d)\n", rc);
	MPI_Abort(MPI_COMM_WORLD, -1);
      }
      
      *power_node = info_power.energy_end - info_power.energy_start;
      
      *power_cpu = 0.0;
      for(int i = 1; i < info_power.num_objs_cpu+1; i++)
	*power_cpu += info_power.energy_sum[i];
      
      *power_drm = 0.0;
      for(int i = info_power.num_objs_cpu+1; i < info_power.num_objs; i++)
	*power_drm += info_power.energy_sum[i];
    }
    return;
  }
}
