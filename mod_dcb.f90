module dcb
  use iso_c_binding
  implicit none

#include "libdcb_def.h"

  interface
     integer(C_INT) function mpi_comm_f2c_f(comm_f, comm_C) bind(C)
       use iso_c_binding
       implicit none
       integer, value :: comm_f
       type(c_ptr) :: comm_C
     end function mpi_comm_f2c_f
       
     integer(C_INT) function mpi_comm_c2f_f(comm_C, comm_f) bind(C)
       use iso_c_binding
       implicit none
       type(c_ptr) :: comm_C
       integer, intent(out) :: comm_f
     end function mpi_comm_c2f_f
  end interface

  ! character(c_char), pointer :: argv_base(:)!, argv
  character(:, c_char), allocatable :: argv_base!, argv
  type(c_ptr), pointer :: argv(:)
  
contains

  subroutine dcb_init_f(whole_comm, status)
    implicit none
    integer, intent(in) :: whole_comm
    integer, intent(out) :: status
    integer(C_INT) whole_comm_f, status_c
    
    interface
       integer(C_INT) function dcb_init_f_im(whole_comm_f) BIND(C)
         use iso_c_binding
         integer, value :: whole_comm_f
       end function dcb_init_f_im
    end interface  

    ! whole_comm_f = whole_comm
    status_c = dcb_init_f_im(whole_comm)
    status = status_c
    
  end subroutine dcb_init_f

  subroutine dcb_set_preset_f(preset_No, load, status)
    implicit none
    integer, intent(in) :: preset_No, load
    integer, intent(out) :: status
    integer(C_INT) preset_No_c, load_c, status_c
    
    interface
       integer(C_INT) function dcb_set_preset(preset_No_c, load_c) BIND(C)
         use iso_c_binding
         integer(C_INT), value :: preset_No_c, load_c
       end function dcb_set_preset
    end interface  

    load_c = load
    preset_No_c = preset_No
    status_c = dcb_set_preset(preset_No_c, load_c)
    status = status_c
    
  end subroutine dcb_set_preset_f
  
  subroutine dcb_balance_f(load, status)
    implicit none
    integer, intent(in) :: load
    integer, intent(out) :: status
    integer(C_INT) load_c, status_c
    
    interface
       integer(C_INT) function dcb_balance(load_c) BIND(C)
         use iso_c_binding
         integer(C_INT), value :: load_c
       end function dcb_balance
    end interface  

    load_c = load
    status_c = dcb_balance(load_c)
    status = status_c
    
  end subroutine dcb_balance_f

  subroutine dcb_balance_preset_f(preset_No, status)
    implicit none
    integer, intent(in) :: preset_No
    integer, intent(out) :: status
    integer(C_INT) preset_No_c, status_c
    
    interface
       integer(C_INT) function dcb_balance_preset(preset_No_c) BIND(C)
         use iso_c_binding
         integer(C_INT), value :: preset_No_c
       end function dcb_balance_preset
    end interface  

    preset_No_c = preset_No
    status_c = dcb_balance_preset(preset_No_c)
    status = status_c
    
  end subroutine dcb_balance_preset_f

  subroutine dcb_verbose_f(header, status)
    implicit none
    character(len=*), intent(in) :: header
    integer, intent(out) :: status
    character(:, C_char), allocatable ::  header_c
    integer(C_INT) status_c

    interface
       integer(C_INT) function dcb_verbose(header_c) BIND(C)
         use iso_c_binding
         ! character(*, C_char), intent(in) :: header_c
         character :: header_c(*)
       end function dcb_verbose
    end interface
    
    header_c = header//C_null_char !F2003 standard
    status_c = dcb_verbose(header_c)
    status = status_c

  end subroutine dcb_verbose_f

  subroutine dcb_free_f(status)
    implicit none
    integer, intent(out) :: status
    integer(C_INT) status_c

    interface
       integer(C_INT) function dcb_free() BIND(C)
         use iso_c_binding
       end function dcb_free
    end interface
       
    status_c = dcb_free()
    status = status_c
    
  end subroutine dcb_free_f

  subroutine dcb_start_power_measure_f(header_fname, time_guess, status)
    implicit none
    character(len=*), intent(in) :: header_fname
    integer, intent(in)  :: time_guess
    integer, intent(out) :: status
    integer(C_INT) status_C, time_guess_C
    character(:, C_char), allocatable :: header_fname_C

    interface
       integer(C_INT) function dcb_start_power_measure(header_fname, time_guess) BIND(C)
         use iso_c_binding
         implicit none
         character :: header_fname(*)
         integer(c_int), value :: time_guess
       end function dcb_start_power_measure
    end interface

    ! write(*, *) 'Check args in interface ', header_fname(1:6), ' ', time_guess(1:2)

    header_fname_C = header_fname//C_null_char !F2003 standard
    time_guess_C = time_guess
    status_C = dcb_start_power_measure(header_fname_C, time_guess_C)
    status = status_C
    
  end subroutine dcb_start_power_measure_f

  subroutine dcb_stop_power_measure_f(header_fname, time_guess, num_sockets, pos_socket, num_drms, pos_drm, status)
    implicit none
    character(len=*), intent(in) :: header_fname
    integer, intent(in)          :: time_guess, num_sockets, num_drms
    integer, intent(in)          :: pos_socket(:), pos_drm(:)
    integer, intent(out)         :: status
    integer i
    integer(C_INT) status_C, time_guess_C, num_sockets_C, num_drms_C
    integer(C_INT), allocatable, target :: pos_socket_C(:), pos_drm_C(:)
    character(:, C_char), allocatable :: header_fname_C
    
    interface
       integer(C_INT) function dcb_stop_power_measure(header_fname, time_guess, num_sockets, pos_socket, num_drms, pos_drm) BIND(C)
         use iso_c_binding
         implicit none
         character :: header_fname(*)
         integer(c_int), value :: time_guess, num_sockets, num_drms
         type(c_ptr), value :: pos_socket, pos_drm
       end function dcb_stop_power_measure
    end interface

    header_fname_C = header_fname//C_null_char !F2003 standard
    time_guess_C = time_guess
    num_sockets_C = num_sockets
    num_drms_C = num_drms
    allocate(pos_socket_C(num_sockets), pos_drm_C(num_drms))
    do i = 1, num_sockets
       pos_socket_C(i) = pos_socket(i) - 1
    enddo
    do i = 1, num_drms
       pos_drm_C(i) = pos_drm(i) - 1
    enddo
    status_C = dcb_stop_power_measure(header_fname_C, time_guess_C, num_sockets_C, c_loc(pos_socket_C), num_drms_C, c_loc(pos_drm_C))
    status = status_C

    deallocate(pos_socket_C, pos_drm_C)
    
  end subroutine dcb_stop_power_measure_f
  
  subroutine dcb_output_param_f(param, fname, comm_output, status)
    use iso_c_binding
    implicit none
    integer, intent(in)  :: param, comm_output
    integer, intent(out) :: status
    character(len=*), intent(in) :: fname
    type(c_ptr) comm_output_C
    integer(C_INT) status_C, param_C, status_dum
    character(:, C_char), allocatable :: fname_C

    ! integer me_proc, ierr

    interface
       integer(C_INT) function dcb_output_param(param, fname, comm_output) BIND(C)
         use iso_c_binding
         implicit none
         character :: fname(*)
         integer(c_int), value :: param
         type(c_ptr) :: comm_output
       end function dcb_output_param
    end interface
    
    ! call MPI_Comm_rank(comm_output, me_proc, ierr)

    fname_C = fname//C_null_char  !F2003 standard
    param_C = param
    ! write(*, *) 'Check comm bef', me_proc, comm_output, comm_output
    status_dum = mpi_comm_f2c_f(comm_output, comm_output_C)
    ! write(*, *) 'Check comm aft', me_proc, comm_output, comm_old
    status_C = dcb_output_param(param_C, fname_C, comm_output_C)
    ! write(*, *) 'Check output_param aft', me_proc
    status = status_C
    
  end subroutine dcb_output_param_f

  subroutine dcb_create_new_comm_f(fname, comm_old, comm_new, status)
    use iso_c_binding
    implicit none
    integer, intent(in)  :: comm_old
    character(len=*), intent(in) ::  fname
    integer, intent(out) :: comm_new, status
    integer len
    type(c_ptr) comm_old_C, comm_new_C
    integer(C_INT) status_C, param_C, status_dum
    character(:, C_char), allocatable :: fname_C
    
    integer me_proc, ierr

    interface
       integer(C_INT) function dcb_create_new_comm(fname, comm_old, comm_new) BIND(C)
         use iso_c_binding
         implicit none
         character :: fname(*)
         type(c_ptr) :: comm_old, comm_new
       end function dcb_create_new_comm
    end interface

    call MPI_Comm_rank(comm_old, me_proc, ierr)

    ! len = len_trim(fname)
    ! fname_C = fname(1:len)//C_null_char  !F2003 standard
    fname_C = fname//C_null_char  !F2003 standard
    ! write(*, *) 'Check comm f2c bef', me_proc, comm_old
    status_dum = mpi_comm_f2c_f(comm_old, comm_old_C)
    ! write(*, *) 'Check create_comm bef', me_proc, comm_old
    status_C = dcb_create_new_comm(fname_C, comm_old_C, comm_new_C)
    ! write(*, *) 'Check comm c2f bef', me_proc
    status_dum = mpi_comm_c2f_f(comm_new_C, comm_new)
    status = status_C
    
  end subroutine dcb_create_new_comm_f

  subroutine dcb_create_new_comm_incproc_f(fname, comm_old, comm_new, status)
    use iso_c_binding
    implicit none
    integer, intent(in)  :: comm_old
    character(len=*), intent(in) ::  fname
    integer, intent(out) :: comm_new, status
    integer i, len, sum_arguments, nstatus, ptr
    type(c_ptr) comm_old_C, comm_new_C
    integer(C_INT) status_C, param_C, status_dum
    character(:, C_char), allocatable :: fname_C, fname_vcoord_C
    integer me_proc, num_procs, ierr
    integer(c_int) argc
    character(1000) :: read_strg

    interface
       ! integer(C_INT) function dcb_create_new_comm_incproc(fname, comm_old, comm_new, argc, argv) BIND(C)
       !   use iso_c_binding
       !   implicit none
       !   character :: fname(*)
       !   type(c_ptr) :: comm_old, comm_new
       !   integer(c_int), value :: argc
       !   type(c_ptr) :: argv(*)
       ! end function dcb_create_new_comm_incproc
       integer(C_INT) function dcb_create_new_comm_incproc(fname, comm_old, comm_new, argc, base_argv) BIND(C)
         use iso_c_binding
         implicit none
         character :: fname(*)
         type(c_ptr) :: comm_old, comm_new
         integer(c_int), value :: argc
         character(c_char) :: base_argv(*)
       end function dcb_create_new_comm_incproc
    end interface

    argc = command_argument_count()
    sum_arguments = 0
    do i = 0, argc
       call get_command_argument(i, read_strg, len, nstatus)
       sum_arguments = sum_arguments + len
    enddo
    ! allocate(argv_base(sum_arguments+argc+1), argv(0:argc+1))
    allocate(argv(0:argc+1))
    allocate(character(len=sum_arguments+argc+1) :: argv_base)
    ! write(*, *) 'Check size argv', sum_arguments
    ptr = 1
    do i = 0, argc
       call get_command_argument(i, read_strg, len, nstatus)
       ! write(*, *) 'Check get ', i, read_strg(1:len), len, read_strg(1:len)//C_null_char
       argv_base(ptr:ptr+len) = read_strg(1:len)//C_null_char
       ! write(*, *) 'Check arguments', i, argv_base(ptr:ptr+len)
       ! argv(i) = c_loc(argv_base(ptr))
       ptr = ptr + len + 1
    enddo
    argv(argc+1) = c_null_ptr
       
    call MPI_Comm_rank(comm_old, me_proc, ierr)
    
    ! len = len_trim(fname)
    ! fname_C = fname(1:len)//C_null_char  !F2003 standard
    fname_C = fname//C_null_char  !F2003 standard
    ! write(*, *) 'Check comm f2c bef', me_proc, comm_old
    status_dum = mpi_comm_f2c_f(comm_old, comm_old_C)
    ! write(*, *) 'Check create_comm bef', me_proc, comm_old
    ! status_C = dcb_create_new_comm_incproc(fname_C, comm_old_C, comm_new_C, argc, argv)
    status_C = dcb_create_new_comm_incproc(fname_C, comm_old_C, comm_new_C, argc, argv_base)
    ! write(*, *) 'Check comm c2f bef', me_proc
    status_dum = mpi_comm_c2f_f(comm_new_C, comm_new)
    ! call MPI_Comm_size(comm_new, num_procs, ierr)
    ! write(*, *) 'Check inside ', num_procs
    status = status_C
    
  end subroutine dcb_create_new_comm_incproc_f
end module dcb
