#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "dcb_common.h"

int extract_int(char const *str_in, int **int_out)
{
  int i, j, num_int;
  int flag_int;
  
  num_int = 0;
  flag_int = 0;
  i = 0;
  while(str_in[i] != '\0'){
    if((int)'0' <=(int)str_in[i] && (int)str_in[i] <= (int)'9'){
      if(flag_int == 0){
	num_int++;
	flag_int = 1;
      }
    }else{
      flag_int = 0;
    }  
    i++;
  }
  
  if(num_int != 0){
    if((*int_out = (int *)malloc(sizeof(int) * num_int)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed in extract_int\n");
      MPI_Abort(MPI_COMM_WORLD, -1);
    }

    for(i=0; i<num_int; i++){
      (*int_out)[i] = -1;
    }
    
    i = 0;
    j = 0;
    while(1){
      sscanf(&str_in[i], "%d", &(*int_out)[j]);
      j++;
      if(j == num_int) break;
      
      //Skip to next argument
      while((int)'0' <=(int)str_in[i] && (int)str_in[i] <= (int)'9'){ //Loop while str_int[i] is integer value
	i++;
      }
      while((int)str_in[i] <= (int)'0'-1 || (int)'9'+1 <= (int)str_in[i]){ //Loop while str_int[i] is not integer value
	i++;
      }

    }

    qsort(*int_out, num_int, sizeof(int), ascending_order);
    
  }else{
    *int_out = NULL;
  }

  return num_int;
}

int ascending_order(const void *a, const void *b)
{
    return *(int*)a - *(int*)b;
}

int calc_digit(int val){
  int digit;
  
  digit = 0;
  while(val != 0){
    val /= 10;
    digit++;
  }
  
  return digit;
}

void quicksort(float *master, int *slave, int n){
  int i, j, flag;
  int temp_i;
  float pivot, temp_r;  

  if(n <= 1) return;

  flag = 0;
  temp_r = master[0];
  for(i=1; i<n; i++){
    if(temp_r != master[i]){
      flag = 1;
      break;
    }
  }

  if(! flag) return;
  
  pivot = master[0];
  for(i=1; i<n; i++)
    if(pivot > master[i]){
      pivot = master[i];
      break;
    }
    
  i = 0;
  j = n-1;
  while (i <= j){
    while (master[i] > pivot && i < n-1){
      i = i + 1;
    }
    
    while (pivot >= master[j] && j > 0){
      j = j - 1;
    }

    if (i > j) break;

    temp_r = master[i];
    master[i] = master[j];
    master[j] = temp_r;

    temp_i = slave[i];
    slave[i] = slave[j];
    slave[j] = temp_i;

    i = i + 1;
    j = j - 1;
  }
  
  if (i >= 2) quicksort(&master[0], &slave[0], i);
  if (n - i >= 2) quicksort(&master[i], &slave[i], n - i);
  
}

void quicksort_int(int *master, int *slave, int n){
  int i, j, flag;
  int temp_i;
  int pivot, temp_r;  

  if(n <= 1) return;

  flag = 0;
  temp_r = master[0];
  for(i=1; i<n; i++){
    if(temp_r != master[i]){
      flag = 1;
      break;
    }
  }

  if(! flag) return;
  
  pivot = master[0];
  for(i=1; i<n; i++)
    if(pivot < master[i]){
      pivot = master[i];
      break;
    }
    
  i = 0;
  j = n-1;
  while (i <= j){
    while (master[i] < pivot && i < n-1){
      i = i + 1;
    }
    
    while (pivot <= master[j] && j > 0){
      j = j - 1;
    }

    if (i > j) break;

    temp_r = master[i];
    master[i] = master[j];
    master[j] = temp_r;

    temp_i = slave[i];
    slave[i] = slave[j];
    slave[j] = temp_i;

    i = i + 1;
    j = j - 1;
  }
  
  if (i >= 2) quicksort_int(&master[0], &slave[0], i);
  if (n - i >= 2) quicksort_int(&master[i], &slave[i], n - i);
  
}

void *remalloc(void *old_array, int const dsize, int const old_size, int const new_size){
  void *new_array;

  new_array = (void *)malloc(new_size * dsize);
  
  if(new_array != NULL){
    memcpy(new_array, old_array, dsize*old_size);
    if(old_array != NULL) free(old_array);
  }

  return new_array;
}
