#ifndef _LIBDCB_H
#define _LIBDCB_H     1

#include <mpi.h>
#include "libdcb_def.h"

int dcb_init(MPI_Comm const comm_whole);
int dcb_set_max_presets(int const mnp);
int dcb_set_preset(int const preset_No, int const load);
int dcb_balance(int const load);
int dcb_balance_preset(int const preset_No);
int dcb_verbose(char const *header);
int dcb_free();

int dcb_start_power_measure(char const *fname, int const time_guess);
int dcb_stop_power_measure(char const *fname, int const time_guess, int const count_cpu, int const *pos_cpu,
	  		   int const count_mem, int const *pos_mem);

int dcb_output_param(int const param, char const *fname, MPI_Comm const *comm_output);
int dcb_create_new_comm(char const *fname, MPI_Comm const *MPI_Old_comm, MPI_Comm *MPI_New_comm);
int dcb_create_new_comm_incproc(char const *fname, MPI_Comm const *MPI_Old_comm, MPI_Comm *MPI_New_comm,
				int argc, char *strg_argv);

#endif
