program main
  use mpi
  use omp_lib
  use dcb
  implicit none
  integer load, i
  integer, allocatable :: loads(:)
  integer proc, me_proc, num_procs, MPI_ierr, err, new_comm
  integer provided
  integer :: required = MPI_THREAD_SERIALIZED
  integer :: flag_comm
  integer, parameter :: num_sockets=2, num_drms=2
  integer :: pos_socket(num_sockets) = (/ 12, 13 /), pos_drm(num_drms) = (/ 10, 11 /)
  integer :: fo = 10
  character(1000) :: fname
 
  call MPI_Init_thread(required, provided, mpi_ierr)
  if(provided < required) then
     if(me_proc == 0) write(*, *) "Requred mpi operation type is not supported\n"
     call MPI_Abort(MPI_COMM_WORLD, -1, mpi_ierr)
  endif
 
  call MPI_Barrier(MPI_COMM_WORLD, mpi_ierr)

  !Creating new communicator for load-balancing among nodes (swap rank IDs)
  ! call dcb_create_new_comm_f("test_neworder_16.txt", MPI_COMM_WORLD, new_comm, mpi_ierr)　
  !Creating new communicator for load-balancing among nodes (spawn, finalize and swap rank IDs)
  call dcb_create_new_comm_incproc_f("test_neworder_16.txt", MPI_COMM_WORLD, new_comm, flag_comm)
  ! new_comm = MPI_COMM_WORLD

  if(flag_comm == 1) then
     
     call MPI_Barrier(new_comm, mpi_ierr)
     
     call MPI_Comm_rank(new_comm, me_proc, mpi_ierr)
     call MPI_Comm_rank(new_comm, num_procs, mpi_ierr)
     ! write(*, *) 'Check newcomm, num_procs = ', num_procs, ', me_proc =', me_proc
     
     ! allocate(loads(num_procs))
     ! call get_command_argument(1, fname)
     ! fname = trim(fname)
     ! open(fo, file = fname)
     ! do proc = 1, num_procs
     !    read(fo, *) i, loads(proc)
     ! enddo
     ! load = loads(me_proc)
     
     load = 10 * (me_proc + 1)
     ! if(me_proc == 0) load = 0
     ! if(me_proc == 1) load = 0
     
     call dcb_init_f(new_comm, err)
     
     ! call dcb_set_preset_f(1, 1, err)
     ! call dcb_set_preset_f(2, load, err)
     
     ! call dcb_verbose_f("Before balance", err)
     call dcb_balance_f(load, err)
     call dcb_verbose_f("After balance", err)
     
     ! call dcb_balance_preset_f(1, err);
     ! call dcb_verbose_f("Even balance", err);
     
     ! call dcb_balance_preset_f(2, err);
     ! call dcb_verbose_f("Re-balancing with preset", err);
     
     ! call MPI_Barrier(MPI_COMM_WORLD, err)
     ! call dcb_start_power_measure_f("sample", "60", err)
     ! call sleep(5)
     ! call dcb_end_power_measure_f("sample", "60", num_sockets, pos_socket, num_drms, pos_drm, err)
     
     call MPI_Barrier(new_comm, err)
     call dcb_free_f(err)

  endif
  
  call MPI_Barrier(MPI_COMM_WORLD, MPI_ierr)
  call MPI_Finalize(MPI_ierr)
  
end program main
  
