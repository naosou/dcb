#include "dcb_intra.h"
#include "libdcb.h"
#include "dcb_common.h"

#include <limits.h>

int flag_verbose, flag_enable_IO = 1, max_num_presets;
struct info_cpu_t info_cpu = {.flag_alloc = 0};
struct info_bind_t info_bind = {.flag_alloc = 0};
struct preset_t *presets;

MPI_Comm intra_comm, inter_comm, whole_comm;

#ifdef _measure_time
int count_reserve = 0, count_bind = 0;
double time_init = 0.0, time_reserve = 0.0, time_bind = 0.0, time_preset = 0.0;
#endif

pid_t gettid(void) {
    return syscall(SYS_gettid);
}

int dcb_init(MPI_Comm const whole_comm_arg){
  int me_proc_whole, me_proc_intra;
  int nprocs_whole, nprocs_intra, nnodes;
  int *mapped_list;
  int err;
  // int MPI_err;

#ifdef _measure_time
  time_init = omp_get_wtime();
#endif

  whole_comm = whole_comm_arg;
  MPI_Comm_rank(whole_comm, &me_proc_whole);

  if(omp_in_parallel()){
    if(me_proc_whole == 0) fprintf(stderr, "DCB : Calling \"dcb_init\" in the parallel have is invalid.\n");
    return dcb_unsupport; 
  }
  
  intra_comm = MPI_COMM_NULL;
  inter_comm = MPI_COMM_NULL;
  //Create communicator among intra-processes(inside of a node) and inter-processes
  err = create_intra_comm(whole_comm, &intra_comm, &inter_comm);

  MPI_Comm_size(whole_comm, &nprocs_whole);
  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_rank(intra_comm, &me_proc_intra);
  // if(me_proc_intra == 0) MPI_Comm_size(inter_comm, &nnodes);
  MPI_Comm_size(inter_comm, &nnodes);
  // ncores = get_ncores(intra_comm);

  MPI_Comm_rank(whole_comm, &me_proc_whole);

  err = get_cpuinfo(&mapped_list, whole_comm, intra_comm); //Get a default setting of binding cores.
  err = get_enval(whole_comm); //Get environment variables
  err = create_valid_table(intra_comm, inter_comm); //Create a table of valid cores based on the default settings and environment variables

  //Creating array for presets
  if((presets = (struct preset_t *)malloc(sizeof(struct preset_t) * max_num_presets)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  for(int i=1; i<max_num_presets; i++)
    presets[i].flag = 0;

  set_zero_preset(&(presets[0]), mapped_list, intra_comm);
  free(mapped_list);

#ifdef _enable_PowerAPI
  init_power_api(me_proc_intra);
#endif
  
  MPI_Barrier(whole_comm);
  if(flag_verbose && me_proc_whole == 0) printf("DCB : Number of processes = %d, nodes = %d, and processes per node = %d\n", \
						                                                             nprocs_whole, nnodes, nprocs_intra);

/*   MPI_Barrier(whole_comm); */
/* #pragma omp parallel default(none) shared(whole_comm) private(err) */
/*   { */
/*     err = verbose_bindinfo("1", whole_comm); */
/* #pragma omp barrier */
/*   } */
  
#ifdef _measure_time
  time_init = omp_get_wtime() - time_init;
#endif

  return err;  
}

int dcb_init_f_im(MPI_Fint const whole_comm_f){
  int err;
  MPI_Comm whole_comm;

  whole_comm = MPI_Comm_f2c(whole_comm_f);
  err = dcb_init(whole_comm);

  return err;
}

int dcb_set_max_presets(int const mnp){
  int err;

  err = dcb_success;
  
  if(!info_cpu.flag_alloc || !info_bind.flag_alloc){
    fprintf(stderr, "DCB : \"dcb_init\" is not called. Please call dcb_init(MPI_Comm) in the first\n");
    err = dcb_uninitialized;
  }else{
    max_num_presets = mnp;
  }
  
  return err;
}

int dcb_set_preset(int const preset_No, int const load){
  int err;
  
  err = dcb_success;

  if(preset_No == 0){
    fprintf(stderr, "DCB : Preset number 0 is reserved for DCB library. Please use more than 1.\n");
    return dcb_invalid_preset_no;
  
  }else if(preset_No >= max_num_presets){
    fprintf(stderr, "DCB : Invalid preset No %d. The preset No must be smaller than the DCB_MAX_NUM_PRESETS = %d\n", \
                                                                                                        preset_No, max_num_presets);
    return dcb_invalid_preset_no;
  }

  if(flag_verbose){
    if(presets[preset_No].flag){
      printf("DCB : Updating preset No %d\n", preset_No);
    }else{
      printf("DCB : Initializing preset No %d\n", preset_No);
    }
  }

  presets[preset_No].flag = 1;
  presets[preset_No].load = load;    

#ifdef _measure_time
  count_reserve++;
  time_reserve = time_reserve - omp_get_wtime();
#endif
  if(info_bind.mode_pr == 1){ //Power aware mode
    // err = reserve_cores(1, &presets[preset_No].num_cpus, &presets[preset_No].num_max_thrds, &presets[preset_No].reserve_list,
    //                                                                                         intra_comm, inter_comm, whole_comm);
    if((presets[preset_No].reserve_list = (int *)malloc(sizeof(int) * presets[0].num_cpus)) == NULL){ //copy reserve list of initial condition
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    for(int i=0; i<presets[0].num_cpus; i++)
      presets[preset_No].reserve_list[i] = presets[0].reserve_list[i];
    presets[preset_No].num_cpus = presets[0].num_cpus;
    err = reserve_cores_pa(load, &presets[preset_No].num_cpus, &presets[preset_No].num_max_thrds, &presets[preset_No].reserve_list, \
			   intra_comm, inter_comm, whole_comm);
  }else if(info_bind.mode_pr == 2){ //Focusing on memory mode
    // err = reserve_cores(1, &presets[preset_No].num_cpus, &presets[preset_No].num_max_thrds, &presets[preset_No].reserve_list,
    //                                                                                         intra_comm, inter_comm, whole_comm);
    if((presets[preset_No].reserve_list = (int *)malloc(sizeof(int) * presets[0].num_cpus)) == NULL){ //copy reserve list of initial condition
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    for(int i=0; i<presets[0].num_cpus; i++)
      presets[preset_No].reserve_list[i] = presets[0].reserve_list[i];
    presets[preset_No].num_cpus = presets[0].num_cpus;
    err = reserve_cores_mem(load, &presets[preset_No].num_cpus, &presets[preset_No].num_max_thrds, &presets[preset_No].reserve_list, \
			   intra_comm, inter_comm, whole_comm);
  }else{
    err = reserve_cores(load, &presets[preset_No].num_cpus, &presets[preset_No].num_max_thrds, &presets[preset_No].reserve_list, \
		      intra_comm, inter_comm, whole_comm);
  }
#ifdef _measure_time
  time_reserve = time_reserve + omp_get_wtime();
#endif

  return err;
}

int dcb_balance(int const load){
  int me_proc_whole, err;
  
  if(!info_cpu.flag_alloc || !info_bind.flag_alloc){
    fprintf(stderr, "DCB : \"dcb_init\" is not called. Please call dcb_init(MPI_Comm) in the first\n");
    err = dcb_uninitialized; 
  }

  if(info_bind.flag_disable){
    MPI_Comm_rank(whole_comm, &me_proc_whole);
    if(flag_verbose && me_proc_whole == 0 && flag_enable_IO) fprintf(stderr, "DCB : DCB is disabled\n");
    flag_enable_IO = 0;
    err = dcb_success; 

  }else if(omp_in_parallel()){

    MPI_Comm_rank(whole_comm, &me_proc_whole);
    if(me_proc_whole == 0) fprintf(stderr, "DCB : Calling \"dcb_balance\" routine in the parallel region have not supported, yet.\n");
    err = dcb_unsupport; 

  }else{

    err = balancing_core(load, intra_comm, inter_comm);
    MPI_Barrier(intra_comm);

  }
  
  return err;  
}

int dcb_balance_preset(int const preset_No){
  int me_proc_whole, err;
  
  MPI_Comm_rank(whole_comm, &me_proc_whole);

  if(!info_cpu.flag_alloc || !info_bind.flag_alloc){
    fprintf(stderr, "DCB : \"dcb_init\" is not called. Please call dcb_init(MPI_Comm) in the first\n");
    err = dcb_uninitialized; 
  }

  if(info_bind.flag_disable){
    if(flag_verbose && me_proc_whole == 0 && flag_enable_IO) fprintf(stderr, "DCB : DCB is disabled\n");
    flag_enable_IO = 0;
    err = dcb_success;
    
  }else if(omp_in_parallel()){
    if(me_proc_whole == 0) fprintf(stderr, "DCB : Calling \"dcb_balance_preset\" routine in the parallel region have not supported, yet.\n");
    err = dcb_unsupport; 

  }else if(!presets[preset_No].flag){
    if(me_proc_whole == 0)
      fprintf(stderr, "DCB : Calling \"dcb_balance_preset\" with uninitialized preset No.\nPlease call dcb_init_preset with preset No %d\n",\
	      preset_No);
    err = dcb_invalid_preset_no;

  }else{

#ifdef _measure_time
    count_bind++;
    time_bind = time_bind - omp_get_wtime();
#endif
    err = bind_threads_to_cpu(presets[preset_No].num_cpus, presets[preset_No].num_max_thrds, presets[preset_No].reserve_list, intra_comm);
#ifdef _measure_time
    time_bind = time_bind + omp_get_wtime();
#endif

    MPI_Barrier(intra_comm);
  }

  return err;
}

int dcb_verbose(char const *header){
  int err;

  if(!info_cpu.flag_alloc || !info_bind.flag_alloc){
    fprintf(stderr, "DCB : \"dcb_init\" is not called. Please call dcb_init(MPI_Comm) in the first\n");
    err = dcb_uninitialized; 
  }

  err = verbose_bindinfo(header, whole_comm);
  fflush(stdout);
  return err;
}

int dcb_free(){
  int me_proc_whole, me_proc_intra, err;

  err = dcb_success;

  MPI_Comm_rank(whole_comm, &me_proc_whole);

  if(!info_cpu.flag_alloc || !info_bind.flag_alloc){
    fprintf(stderr, "DCB : \"dcb_init\" is not called. Please call dcb_init(MPI_Comm) in the first\n");
    err = dcb_uninitialized; 
  }

  if(omp_in_parallel()){
    if(me_proc_whole == 0) fprintf(stderr, "DCB : Calling \"dcb_free\" in the parallel have is invalid.\n");
    return dcb_unsupport; 
  }
 
  info_cpu.flag_alloc = 0;
  free(info_cpu.ptr_core_to_cpu);
  free(info_cpu.core_to_cpu);
  free(info_cpu.ptr_socket_to_core);
  free(info_cpu.socket_to_core);
  free(info_cpu.mapped_list);
  free(info_cpu.num_cores_socket);
  free(info_cpu.list_cpus_online);
  if(info_cpu.num_cpus_offline) free(info_cpu.list_cpus_offline);

  info_bind.flag_alloc = 0;
  if(info_bind.num_excl_cpus)  free(info_bind.excl_list_cpu);
  if(info_bind.num_excl_cores) free(info_bind.excl_list_core);
  free(info_bind.valid_cpu);
  free(info_bind.reserve_list);

  MPI_Comm_rank(intra_comm, &me_proc_intra);
  MPI_Barrier(whole_comm);

#ifdef _enable_PowerAPI
  free_power_api(me_proc_intra);
  MPI_Barrier(whole_comm);
#endif
  
  if(me_proc_intra == 0)
    if(MPI_Comm_free(&inter_comm) != MPI_SUCCESS){ 
      fprintf(stderr, "DCB : Failed MPI_comm_free\n");
      MPI_Abort(MCW, -1);
    }
  
  if(MPI_Comm_free(&intra_comm) != MPI_SUCCESS){ 
    fprintf(stderr, "DCB : Failed MPI_comm_free\n");
    MPI_Abort(MCW, -1);
  }

  finalize_newcomm_incproc();
  
  MPI_Barrier(whole_comm);

#ifdef _measure_time
  if(me_proc_whole == 0){
    printf("DCB : Measured time, init = %f, reserve = %f, bind = %f\n", time_init, time_reserve/count_reserve, time_bind/count_bind);
  }
#endif

  return err;
}

int get_cpuinfo(int **mapped_list, MPI_Comm const whole_comm, MPI_Comm const intra_comm)
{
  int i, id_s, id_c, id_C;
  int err, err_last, ptr, ptr_online, ptr_offline;
  int me_proc, me_proc_intra;
  int *id_socket, *id_core, *count_sockets, *count_cores;
  pid_t pid;
  cpu_set_t mask_affinity;
  // struct cpu_raw_data_t CPUID_raw;
  // struct cpu_id_t CPUID_data;
  FILE *fp;
  char *cmd_lscpu;
  char temp_strg[1000];

  if(info_cpu.flag_alloc == 1) return dcb_success;
  
  MPI_Comm_rank(whole_comm, &me_proc);
  MPI_Comm_rank(intra_comm, &me_proc_intra);
  // printf("Check me_proc_intra = %d\n", me_proc_intra);
  err_last = dcb_success;
  info_cpu.flag_alloc = 1;

  cmd_lscpu = "lscpu -p=\"cpu,core,socket\""; //Executing lscpu to get list of core and socket id.
  if ((fp = popen(cmd_lscpu, "r")) == NULL) {
    // if ((fp = fopen("lscpu.txt", "r")) == NULL) {
    fprintf(stderr, "DCB : Invalid Command \"lscpu\" ! Please check \"lscpu\" command is valid.\n");
    MPI_Abort(MCW, -1);
  }

  info_cpu.num_cpus_online = 0;
  info_cpu.num_cpus = 0;
  info_cpu.num_cores = 0; 
  info_cpu.num_sockets = 0;
  while(fgets(temp_strg, sizeof(temp_strg), fp) != NULL){ //Storing socket and core ID from \"lscpu\" command.
    if(temp_strg[0] != '#'){
      sscanf(temp_strg, "%d, %d, %d", &id_C, &id_c, &id_s);
      if(info_cpu.num_cpus < id_C) info_cpu.num_cpus = id_C;
      if(info_cpu.num_cores < id_c) info_cpu.num_cores = id_c;
      if(info_cpu.num_sockets < id_s) info_cpu.num_sockets = id_s;
      info_cpu.num_cpus_online++; //There is a system that max CPU id not equal number of CPUs. 
    }
  }
  pclose(fp);
  info_cpu.num_cpus++;
  info_cpu.num_cores++;
  info_cpu.num_sockets++;
  info_cpu.num_cpus_offline = info_cpu.num_cpus - info_cpu.num_cpus_online;
  // printf("Check num_cpus = %d, num_cores = %d, num_sockets = %d\n", info_cpu.num_cpus, info_cpu.num_cores, info_cpu.num_sockets);
  
  if((info_cpu.list_cpus_online = (int *)malloc(sizeof(int) * info_cpu.num_cpus_online)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  if((id_core = (int *)malloc(sizeof(int) * info_cpu.num_cpus_online)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  if((id_socket = (int *)malloc(sizeof(int) * info_cpu.num_cpus_online)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  
  cmd_lscpu = "lscpu -p=\"cpu,core,socket\""; //Executing lscpu to get list of core and socket id.
  if ((fp = popen(cmd_lscpu, "r")) == NULL) {
    fprintf(stderr, "DCB : Invalid Command \"lscpu\" ! Please check \"lscpu\" command is valid.\n");
    MPI_Abort(MCW, -1);
  }
  fseek(fp, 0L, SEEK_SET);
  i = 0;
  while(fgets(temp_strg, sizeof(temp_strg), fp) != NULL){ //Storing socket and core ID from \"lscpu\" command.
    // printf("Check fgets strg %s\n", temp_strg);
    if(temp_strg[0] != '#'){
      sscanf(temp_strg, "%d, %d, %d", &info_cpu.list_cpus_online[i], &id_core[i], &id_socket[i]);
      i++;
    }
  }
  
  // MPI_Barrier(MCW);
  pclose(fp);
  // fclose(fp);

  //Counting number of cores on each socket for supporting systems which have HBM and assistant cores.
  if((info_cpu.num_cores_socket = (int *)calloc(info_cpu.num_sockets, sizeof(int))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  for(i=0; i<info_cpu.num_cores; i++){
    id_s = id_socket[i];
    info_cpu.num_cores_socket[id_s]++;
  }
  info_cpu.max_cores_socket = 0;
  for(i=0; i<info_cpu.num_sockets; i++)
    if(info_cpu.max_cores_socket < info_cpu.num_cores_socket[i]) info_cpu.max_cores_socket = info_cpu.num_cores_socket[i];
  // info_cpu.max_cores_socket = info_cpu.num_cores / info_cpu.num_sockets;
  info_cpu.logic_HT = info_cpu.num_cpus_online / info_cpu.num_cores;

  //Creating CPU list which are offline.
  if(info_cpu.num_cpus_offline != 0){
    if((info_cpu.list_cpus_offline = (int *)malloc(sizeof(int) * info_cpu.num_cpus_offline)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    ptr_online = 0;
    ptr_offline = 0;
    for(i=0; i<info_cpu.num_cpus; i++){
      // if(me_proc == 1) printf("Check vals i = %d, ptr_online = %d, ID online = %d, ptr_offline = %d\n", i, ptr_online, info_cpu.list_cpus_online[ptr_online], ptr_offline);
      if(i != info_cpu.list_cpus_online[ptr_online]){
	// if(me_proc == 1) printf("True\n");
	info_cpu.list_cpus_offline[ptr_offline] = i;
	ptr_offline++;
      }else{
	// if(me_proc == 1) printf("False\n");
	ptr_online++;
      }
    }
  }else{
    info_cpu.list_cpus_offline = NULL;
  }
    
#ifdef _verbose_debug
  if(me_proc == 0){
    printf("DCB debug : Info of system\n");
    printf("DCB_deubg : Number of online CPUs = %d, cores = %d, sockets = %d, threads per core = %d\n", \
	   info_cpu.num_cpus_online, info_cpu.num_cores, info_cpu.num_sockets, info_cpu.logic_HT);
    printf("DCB_debug : Number of CPUs = %d, Number of offline CPUs = %d\n", info_cpu.num_cpus, info_cpu.num_cpus_offline);
    printf("DCB_deubg : Max number of cores on socket = %d\n", info_cpu.max_cores_socket);
    for(i=0; i<info_cpu.num_sockets; i++)
      printf("DCB_debug : Socket = %d, number of cores = %d\n", i, info_cpu.num_cores_socket[i]);
    printf("DCB_debug : List of online CPUs :");
    for(i=0; i<info_cpu.num_cpus_online; i++)
      printf("%d, ", info_cpu.list_cpus_online[i]);
    printf("\n");
    printf("DCB_debug : List of offline CPUs :");
    for(i=0; i<info_cpu.num_cpus_offline; i++)
      printf("%d, ", info_cpu.list_cpus_offline[i]);
    printf("\n");
    fflush(stdout);
  }
#endif

  if((info_cpu.ptr_core_to_cpu = (int **)malloc(sizeof(int *) * info_cpu.num_cores)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  if((info_cpu.core_to_cpu = (int *)malloc(sizeof(int) * info_cpu.num_cpus)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  ptr = 0;
  for(i=0; i<info_cpu.num_cores; i++){
    info_cpu.ptr_core_to_cpu[i] = &info_cpu.core_to_cpu[ptr];
    ptr += info_cpu.logic_HT;
  }
  if((count_cores = (int *)calloc(info_cpu.num_cores, sizeof(int))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }

  if((info_cpu.ptr_socket_to_core = (int **)malloc(sizeof(int *) * info_cpu.num_sockets)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  if((info_cpu.socket_to_core = (int *)malloc(sizeof(int) * info_cpu.num_cores)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  ptr = 0;
  for(i=0; i<info_cpu.num_sockets; i++){
    info_cpu.ptr_socket_to_core[i] = &info_cpu.socket_to_core[ptr];
    ptr += info_cpu.num_cores_socket[i];
  }
  if((count_sockets = (int *)calloc(info_cpu.num_sockets, sizeof(int))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  for(i=0; i<info_cpu.num_cpus_online; i++){
    id_C = info_cpu.list_cpus_online[i];
    id_c = id_core[i];
    // if(me_proc == 0) printf("Check vals i = %d, id_C = %d, id_c = %d, count_cores = %d\n", i, id_C, id_c, count_cores[id_c]);
    info_cpu.ptr_core_to_cpu[id_c][count_cores[id_c]] = id_C;
    count_cores[id_c]++;
  }
  for(i=0; i<info_cpu.num_cores; i++){
    id_c = id_core[i];
    id_s = id_socket[i];
    info_cpu.ptr_socket_to_core[id_s][count_sockets[id_s]] = id_c;
    count_sockets[id_s]++;
  }
  free(count_cores);
  free(count_sockets);

 // Getting bound info
#pragma omp parallel default(none) shared(err_last, stderr, info_cpu, mapped_list, whole_comm) \
                                  private(i, pid, mask_affinity, err) \
                             firstprivate(me_proc_intra)
  {
    // me_thrd = omp_get_thread_num(); //Getting some system info
    // num_thrds = omp_get_num_threads();
    // err = cpuid_get_raw_data(&CPUID_raw);
    // err = cpu_identify(&CPUID_raw, &CPUID_data);  

    // size_t size_mask;
    // size_mask = CPU_ALLOC_SIZE(CPU_COUNT(&mask_affinity));
    pid = gettid();
    // err = sched_getaffinity(pid, size_mask, &mask_affinity); //Getting scheduled affinity
    err = sched_getaffinity(pid, sizeof(mask_affinity), &mask_affinity); //Getting scheduled affinity
    
#pragma omp master
    {
      MPI_Barrier(whole_comm);
    }
#pragma omp barrier

#pragma omp master
    {
      if((info_cpu.mapped_list = (int *)malloc(sizeof(int) * info_cpu.num_cpus)) == NULL){ //Allocating array for storing mapped list
	fprintf(stderr, "DCB : Memory allocation failed\n");
	MPI_Abort(whole_comm, -1);
      }
      if(((*mapped_list) = (int *)malloc(sizeof(int) * info_cpu.num_cpus)) == NULL){ //Allocating array for storing mapped list
	fprintf(stderr, "DCB : Memory allocation failed\n");
	MPI_Abort(whole_comm, -1);
      }
      for(i=0; i<info_cpu.num_cpus; i++){
	info_cpu.mapped_list[i] = 0; //Initializing array
	(*mapped_list)[i] = 0; //Initializing array
      }
    }
#pragma omp barrier
    
    for(i=0; i<info_cpu.num_cpus; i++){ //Check scheduled affinity and storing flag for array of mapped list
      if(CPU_ISSET(i, &mask_affinity) != 0){
#pragma omp atomic write
	info_cpu.mapped_list[i] = me_proc_intra + 1;
#pragma omp atomic write
	(*mapped_list)[i] = me_proc_intra + 1;
      }
    }
#pragma omp barrier

#pragma omp atomic update
    err_last += err;

  }
  
  info_cpu.num_bound_cpus = 0;
  // MPI_Allreduce(MPI_IN_PLACE, info_cpu.mapped_list, info_cpu.num_cpus, MPI_INT, MPI_SUM, intra_comm);
  MPI_Allreduce(MPI_IN_PLACE, info_cpu.mapped_list, info_cpu.num_cpus, MPI_INT, MPI_MAX, intra_comm);
  for(i=0; i<info_cpu.num_cpus; i++){
    // if(info_cpu.mapped_list[i] != 0){
    //   info_cpu.mapped_list[i] = 1;
    if(info_cpu.mapped_list[i]){
      info_cpu.num_bound_cpus++;
    }
  }
  /* if(me_proc_intra == 0){ */
  /*   printf("Check mapped_list :"); */
  /*   for(int i=0; i<info_cpu.num_cpus; i++) */
  /*     printf("%d, ", info_cpu.mapped_list[i]); */
  /*   printf("\n"); */
  /* } */

#ifdef _verbose_debug
  if(me_proc == 0){
    char *strg_flag;
    if((strg_flag = (char *)malloc(sizeof(char) * (info_cpu.num_cpus * 3 + 1))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }
    for(i=0; i<info_cpu.num_cpus; i++){ //Check default affinity
      if(info_cpu.mapped_list[i]){
	sprintf(&strg_flag[i*3], "T, ");
      }else{
	sprintf(&strg_flag[i*3], "F, ");
      }
    }
    strg_flag[info_cpu.num_cpus*3-2] = '\0';
    printf("DCB debug : Number of bound CPUs = %d, list of enabled CPUs = %s\n", info_cpu.num_bound_cpus, strg_flag); 
    printf("DCB debug : IDs of CPU, core & socket\n");
    printf("DCB debug :  _______________________ \n");
    printf("DCB debug : |    CPU|   core| socket|\n");
    for(i=0; i<info_cpu.num_cores; i++)
      printf("DCB debug : |%7d|%7d|%7d|\n", info_cpu.list_cpus_online[i], id_core[i], id_socket[i]);
    printf("DCB debug :  ----------------------- \n");

    printf("DCB debug : List of CPUs on each core\n");
    for(id_c=0; id_c<info_cpu.num_cores; id_c++){
      printf("DCB debug : Core = %d, CPU list =", id_c);
      for(i=0; i<info_cpu.logic_HT; i++){
	printf(" %d", info_cpu.ptr_core_to_cpu[id_c][i]);
      }
      printf("\n");
    }

    printf("DCB debug : List of CPUs on each socket\n");
    for(id_c=0; id_c<info_cpu.num_sockets; id_c++){
      printf("DCB debug : Socket = %d, core list =", id_c);
      for(i=0; i<info_cpu.num_cores_socket[id_c]; i++){
	printf(" %d", info_cpu.ptr_socket_to_core[id_c][i]);
      }
      printf("\n");
    }

    free(strg_flag);
    fflush(stdout);
  }
#endif

  MPI_Barrier(whole_comm);

  free(id_core);
  free(id_socket);

  if(err_last < 0){
    err = dcb_get_info_failure;
  }else{
    err = dcb_success;
  }
  
  return err;
}

int verbose_bindinfo(char const *header, MPI_Comm const whole_comm){
  int i, count_cpus, *mapped_cpus;
  int thrd, me_thrd, num_thrds;
  static int me_proc;
  int err;
  char *strg_flag;
  pid_t pid;
  cpu_set_t mask_affinity;
  unsigned cpuid;

  if(omp_in_parallel()){
    num_thrds = omp_get_num_threads();
    me_thrd = omp_get_thread_num();
#pragma omp master
    MPI_Comm_rank(whole_comm, &me_proc);
#pragma omp barrier

    // size_t size_mask;
    // size_mask = CPU_ALLOC_SIZE(info_cpu.num_cpus);
    // size_mask = CPU_ALLOC_SIZE(CPU_COUNT(&mask_affinity));
    pid = gettid();
    err = sched_getaffinity(pid, sizeof(mask_affinity), &mask_affinity); //Getting affinity of binding cores
      
    if((strg_flag = (char *)malloc(sizeof(char) * (info_cpu.num_cpus * 3 + 1))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }
    for(i=0; i<info_cpu.num_cpus; i++){ //Check scheduled affinity
      if(CPU_ISSET(i, &mask_affinity) != 0){	      
	sprintf(&strg_flag[i*3], "T, ");
      }else{
	sprintf(&strg_flag[i*3], "F, ");
      }
    }
    strg_flag[info_cpu.num_cpus*3-2] = '\0';
    err = syscall(SYS_getcpu, &cpuid, NULL, NULL);
#pragma omp master
    {
      MPI_Barrier(whole_comm);
    }
#pragma omp barrier
    for(thrd =0; thrd<num_thrds; thrd++){
      if(thrd == me_thrd){
	printf("DCB : %s, Mapped list process = %4d, thread = %4d, bind_list = %s, cpuid = %d\n", header, me_proc, me_thrd, strg_flag, cpuid);
      }
#pragma omp barrier
    }

  }else{ // Not in parallel or single thread
    MPI_Comm_rank(whole_comm, &me_proc);

#pragma omp parallel default(none) shared(count_cpus, strg_flag, mapped_cpus, info_cpu, stderr, whole_comm) private(mask_affinity, err, pid, i)
    {
      pid = gettid();
      err = sched_getaffinity(pid, sizeof(mask_affinity), &mask_affinity);

#pragma omp master
      {
	if((strg_flag = (char *)malloc(sizeof(char) * (info_cpu.num_cpus * 3 + 1))) == NULL){
	  fprintf(stderr, "DCB : Memory allocation failed\n");
	  MPI_Abort(whole_comm, -1);
	}
	if((mapped_cpus = (int *)malloc(sizeof(int) * (info_cpu.num_cpus))) == NULL){
	  fprintf(stderr, "DCB : Memory allocation failed\n");
	  MPI_Abort(whole_comm, -1);
	}
	for(i=0; i<info_cpu.num_cpus; i++){ //Initializing strg_flag and mapped_cpus
	  sprintf(&strg_flag[i*3], "F, ");
	  mapped_cpus[i] = 0;
	}
      }
#pragma omp barrier
      
      for(i=0; i<info_cpu.num_cpus; i++){ //Check scheduled affinity
	if(CPU_ISSET(i, &mask_affinity) != 0){
#pragma omp atomic write	  
	  strg_flag[i*3] = 'T';
#pragma omp atomic write
	  mapped_cpus[i] = 1;
	}
      }
#pragma omp barrier
#pragma omp master
      {
	count_cpus = 0;
	for(i=0; i<info_cpu.num_cpus; i++) //Count number of valid CPUs
	  if(mapped_cpus[i] == 1) count_cpus++;
      }
    }
    strg_flag[info_cpu.num_cpus*3-2] = '\0';
    MPI_Barrier(whole_comm);
    printf("DCB : %s, Mapped list process = %4d, number of CPUs = %d, bind_list = %s\n", header, me_proc, count_cpus, strg_flag);
  }
 
  return err;
}

int get_enval(MPI_Comm const whole_comm){
  char *strg_enval;
  int dcb_status, flag_num_threads_per_cpu;
  
  dcb_status = dcb_success;

  // DCB_VERBOSE
  strg_enval = getenv(ne_verbose);
  if(strg_enval == NULL){
    flag_verbose = 0;
  }
  else{
    sscanf(strg_enval, "%d", &flag_verbose);
  }

  // DCB_NUM_CORES
  strg_enval = getenv(ne_num_cores);
  if(strg_enval == NULL){
    info_bind.num_cores = -1;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.num_cores);
  }

  // DCB_NUM_CPUS_PER_CORE
  strg_enval = getenv(ne_num_cpus_per_core);
  if(strg_enval == NULL){
    info_bind.num_cpus_per_core = info_cpu.logic_HT;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.num_cpus_per_core);
  }
  if(info_bind.num_cpus_per_core > info_cpu.logic_HT){
    printf("Exported number of CPUs per core \"DCB_NUM_CPUS_PER_CORE\" is more than installed hardware. Ignoring\n");
    info_bind.num_cpus_per_core = info_cpu.logic_HT;
  }
  if(info_bind.num_cpus_per_core == 0){
    printf("Exported number of CPUs per core \"DCB_NUM_CPUS_PER_CORE\" is zero. Ignoring\n");
    info_bind.num_cpus_per_core = info_cpu.logic_HT;
  }

  // DCB_NUM_THREADS_PER_CPU
  strg_enval = getenv(ne_num_thrds_per_cpu);
  if(strg_enval == NULL){
    info_bind.num_thrds_per_cpu = 1;
    flag_num_threads_per_cpu = 0;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.num_thrds_per_cpu);
    flag_num_threads_per_cpu = 1;
  }

  // DCB_LIST_CPUIDS_EXCLUDING
  strg_enval = getenv(ne_lcpu_excl);
  if(strg_enval == NULL){
    info_bind.excl_list_cpu = NULL;
    info_bind.num_excl_cpus = 0;
  }
  else{
    info_bind.num_excl_cpus = extract_int(strg_enval, &info_bind.excl_list_cpu);
  }

  // DCB_LIST_COREIDS_EXCLUDING
  strg_enval = getenv(ne_lcore_excl);
  if(strg_enval == NULL){
    info_bind.excl_list_core = NULL;
    info_bind.num_excl_cores = 0;
  }
  else{
    info_bind.num_excl_cores = extract_int(strg_enval, &info_bind.excl_list_core);
  }

  // DCB_DIFFERENT_PROCESS_ON_CORE
  strg_enval = getenv(ne_diffprocs_core);
  if(strg_enval == NULL){
    info_bind.flag_diffprocs_core = 0;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.flag_diffprocs_core);
  }

  // DCB_DIFFERENT_SOCKET_FOR_PROCESS				     
  strg_enval = getenv(ne_proc_diffsockets);
  if(strg_enval == NULL){
    info_bind.flag_proc_diffsockets = 1;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.flag_proc_diffsockets);
  }

  //DCB_BIND_MODE
  strg_enval = getenv(ne_bind_mode);
  if(strg_enval == NULL){
    info_bind.bind_mode = 0;
  }
  else{
    if(strcmp(strg_enval, "node") == 0){
      info_bind.bind_mode = 0;
    }else if(strcmp(strg_enval, "socket") == 0){
      info_bind.bind_mode = 1;
    }else{
      int me_proc;
      MPI_Comm_rank(whole_comm, &me_proc);
      if(me_proc == 0) fprintf(stderr, "DCB : Specify %s or %s for option %s\n", "node", "socket", ne_bind_mode);
      info_bind.bind_mode = 0;      
    }
  }  

  //DCB_MAX_NUM_PRESETS
  strg_enval = getenv(ne_max_num_presets);
  if(strg_enval == NULL){
    max_num_presets = 100;
  }
  else{
    sscanf(strg_enval, "%d", &max_num_presets);
  }

  //DCB_MODE
  strg_enval = getenv(ne_mode_priority);
  if(strg_enval == NULL){
    info_bind.mode_pr = 0;
  }
  else{
    if(strcmp(strg_enval, "speed") == 0){
      info_bind.mode_pr = 0;
    }else if(strcmp(strg_enval, "power") == 0){
      info_bind.mode_pr = 1;
    }else if(strcmp(strg_enval, "memory") == 0){
      info_bind.mode_pr = 2;
    }else if(strcmp(strg_enval, "hybrid") == 0){
      info_bind.mode_pr = 3;
    }else{
      int me_proc;
      MPI_Comm_rank(whole_comm, &me_proc);
      if(me_proc == 0) fprintf(stderr, "DCB : Specify %s, %s or %s for option %s\n", "speed", "power", "hybrid", ne_mode_priority);
      info_bind.mode_pr = 0;      
    }
  }  

  //DCB_MINIMUM_CPUS
  strg_enval = getenv(ne_minimum_cpus);
  if(strg_enval == NULL){
    info_bind.min_cpus = 1;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.min_cpus);
  }

  // DCB_DISABLE				     
  strg_enval = getenv(ne_disable);
  if(strg_enval == NULL){
    info_bind.flag_disable = 0;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.flag_disable);
  }

  // DCB_MEASURING_POWER				     
  strg_enval = getenv(ne_measuring_power);
  if(strg_enval == NULL){
    info_bind.flag_mes_power = 0;
  }
  else{
    sscanf(strg_enval, "%d", &info_bind.flag_mes_power);
  }

  int i, me_proc;
  MPI_Comm_rank(whole_comm, &me_proc);
  if(flag_verbose && me_proc == 0){
    printf("DCB : Verbose mode. Output all environment values\n");

    if(info_bind.flag_disable){
      printf("DCB : DCB is disable (DCB_DISABLE=1)\n");
    }else{
      printf("DCB : DCB is enable (DCB_DISABLE=0)\n");
    }    

    if(info_bind.num_cores < 0){
      printf("DCB : DCB_NUM_CORES = Unset\n");
    }else{
      printf("DCB : DCB_NUM_CORES = %d\n", info_bind.num_cores);
    }

    if(info_bind.num_cpus_per_core == info_cpu.logic_HT){
      printf("DCB : DCB_NUM_CPUS_PER_CORE = %d (Default on this system)\n", info_bind.num_cpus_per_core);
    }else{
      printf("DCB : DCB_NUM_CPUS_PER_CORE = %d\n", info_bind.num_cpus_per_core);
    }

    if(flag_num_threads_per_cpu){
      printf("DCB : DCB_NUM_THREADS_PER_CPU = %d\n", info_bind.num_thrds_per_cpu);
    }else{
      printf("DCB : DCB_NUM_THREADS_PER_CPU = 1 (Unset)\n");
    }
    
    if(info_bind.num_excl_cpus == 0){
      printf("DCB : DCB_LIST_CPUIDS_EXCLUDING = Unset\n");
    }else{    
      printf("DCB : DCB_LIST_CPUIDS_EXCLUDING, num = %d, list = ", info_bind.num_excl_cpus);
      for(i=0; i<info_bind.num_excl_cpus; i++)
	printf("%d, ", info_bind.excl_list_cpu[i]);
      printf("\n");
    }

    if(info_bind.num_excl_cores == 0){
      printf("DCB : DCB_LIST_COREIDS_EXCLUDING = Unset\n");
    }else{    
      printf("DCB : DCB_LIST_COREIDS_EXCLUDING, num = %d, list = ", info_bind.num_excl_cores);
      for(i=0; i<info_bind.num_excl_cores; i++)
	printf("%d, ", info_bind.excl_list_core[i]);
      printf("\n");
    }

    if(info_bind.flag_diffprocs_core == 0){
      printf("DCB : DCB_DIFFERENT_PROCESS_ON_CORE = Off (Default)\n");
    }else{
      printf("DCB : DCB_DIFFERENT_PROCESS_ON_CORE = On\n");
    }

    if(info_bind.flag_proc_diffsockets == 1){
      printf("DCB : DCB_PROCESS_ON_DIFFERENT_SOCKETS = On (Default)\n");
    }else{
      printf("DCB : DCB_PROCESS_ON_DIFFERENT_SOCKETS = Off\n");
    }

    if(info_bind.bind_mode == 0){
      printf("DCB : DCB_BIND_MODE = node (Default)\n");	
    }else{
      printf("DCB : DCB_BIND_MODE = socket\n");	
    }

    if(max_num_presets == 100){
      printf("DCB : DCB_MAX_NUM_PRESETS = 100 (Default)\n");	
    }else{
      printf("DCB : DCB_MAX_NUM_ORESETS = %d\n", max_num_presets);	
    }

    if(info_bind.mode_pr == 0){
      printf("DCB : DCB_MODE_PRIORITY = speed (Default)\n");	
    }else if(info_bind.mode_pr == 1){
      printf("DCB : DCB_MODE_PRIORITY = power\n");	
    }else if(info_bind.mode_pr == 2){
      printf("DCB : DCB_MODE_PRIORITY = memory\n");
    }else if(info_bind.mode_pr == 3){
      printf("DCB : DCB_MODE_PRIORITY = hybrid\n");
    }

    if(info_bind.min_cpus == 1){
      printf("DCB : DCB_MINIMUM_CPUS = 1 (Default)\n");	
    }else{
      printf("DCB : DCB_MINIMUM_CPUS = %d\n", info_bind.min_cpus);	
    }

    if(info_bind.flag_mes_power){
      printf("DCB : DCB enabling power measurement (DCB_DISABLE=1)\n");
    }else{
      printf("DCB : DCB disabling power measurement (DCB_DISABLE=0)\n");
    }    

    fflush(stdout);
  }

  /* int me_proc_intra, me_proc_inter; */
  /* MPI_Comm_rank(intra_comm, &me_proc_intra); */
  /* MPI_Comm_rank(inter_comm, &me_proc_inter); */
  /* if(me_proc_intra == 0){ */
  /*   printf("Check me_proc_inter = %d, DCB_NUM_CORES = %d\n", me_proc_inter, info_bind.num_cores); */
  /*   printf("Check me_proc_inter = %d, DCB_LIST_CPUIDS_EXCLUDING, num = %d, list = ", me_proc_inter, */
  /* 	   info_bind.num_excl_cpus); */
  /*   for(i=0; i<info_bind.num_excl_cpus; i++) */
  /*     printf("%d, ", info_bind.excl_list_cpu[i]); */
  /*   printf("\n"); */
  /* } */
  
  MPI_Barrier(whole_comm);
  
  return dcb_status;
}

void set_zero_preset(struct preset_t *preset, int *mapped_list, MPI_Comm const comm_intra){
  int me_proc_intra;
  
  MPI_Comm_rank(intra_comm, &me_proc_intra);

  /* if(me_proc_intra == 0){ */
  /*   printf("Check mapped_list :"); */
  /*   for(int i=0; i<info_cpu.num_cpus; i++) */
  /*     printf("%d, ", mapped_list[i]); */
  /*   printf("\n"); */
  /* } */
  /* MPI_Barrier(MPI_COMM_WORLD); */
        
  //Creating preset with initial condition
  preset->flag = 1;
  preset->load = 1;
  preset->num_max_thrds = omp_get_max_threads();
  preset->num_cpus = 0;
  for(int i=0; i<info_cpu.num_cpus; i++)
    if(mapped_list[i] == me_proc_intra+1) preset->num_cpus++;
  if((preset->reserve_list = (int *)malloc(sizeof(int) * preset->num_cpus)) == NULL){//Allocating array for reserved list
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(whole_comm, -1);
  }
  int ptr = 0;
  for(int i=0; i<info_cpu.num_cpus; i++)
    if(mapped_list[i] == me_proc_intra+1){
      preset->reserve_list[ptr] = i;
      ptr++;
    }
#ifdef _verbose_debug
  char *strg_list;
  int digit_cpuid = calc_digit(info_cpu.num_cpus);
  if((strg_list = (char *)malloc(sizeof(char) * (preset->num_cpus * (digit_cpuid+2)))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(whole_comm, -1);
  }
  strg_list[preset->num_cpus * (digit_cpuid+2) - 2] = '\0';
  sprintf(&strg_list[0], "%*d", digit_cpuid, preset->reserve_list[0]);
  for(int i=1; i<preset->num_cpus; i++) //Check default affinity
    sprintf(&strg_list[i*(digit_cpuid+2)-2], ", %*d", digit_cpuid, preset->reserve_list[i]);
  printf("DCB debug : Preset0 : Reserved CPUs to process %d, number of CPUs = %d, list = %s\n", me_proc_intra, preset->num_cpus,
	                                                                                                          strg_list);
  free(strg_list);
#endif

}

int create_intra_comm(MPI_Comm const whole_comm, MPI_Comm *intra_comm, MPI_Comm *inter_comm){
  int i, proc;
  int nprocs, me_proc, me_proc_intra;
  static int nprocs_inter;
  int color, key;
  int *list_ranks, *list_ranks_sub;
  int dcb_status;
  MPI_Group grp_whole, grp_inter;

  dcb_status = dcb_success;
  
  MPI_Comm_size(whole_comm, &nprocs);
  MPI_Comm_rank(whole_comm, &me_proc);
  
  color = create_color_id(whole_comm);
  // printf("Check color, me_proc = %d, color = %d\n", me_proc, color);
  key = me_proc;

  // MPI_Barrier(whole_comm);
  // MPI_Abort(whole_comm, -1);
  
  if(MPI_Comm_split(whole_comm, color, key, intra_comm) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_split\n");
    MPI_Abort(MCW, -1);
  }
  MPI_Comm_rank(*intra_comm, &me_proc_intra);

  list_ranks_sub = (int *)malloc(nprocs * sizeof(int));
  MPI_Allgather(&me_proc_intra, 1, MPI_INT, list_ranks_sub, 1, MPI_INT, whole_comm);
  /* printf("Check split rank, %d, %d\n", me_proc, me_proc_intra); */
  /* MPI_err = MPI_Barrier(whole_comm); */
  /* if(me_proc == 0){ */
  /*   printf("Check list_sub"); */
  /*   for(proc=0; proc<nprocs; proc++) printf(" %d", list_ranks_sub[proc]); */
  /*   printf("\n"); */
  /* } */
  /* MPI_err = MPI_Barrier(whole_comm); */
  
  nprocs_inter = 0;
  for(proc=0; proc<nprocs; proc++)
    if(list_ranks_sub[proc] == 0)
      nprocs_inter++;
  // printf("Check nprocs_inter = %d\n", nprocs_inter);

  list_ranks = (int *)malloc(nprocs_inter * sizeof(int));
  i = 0;
  for(proc=0; proc<nprocs; proc++)
    if(list_ranks_sub[proc] == 0){
      list_ranks[i] = proc;
      i++;
    }
  /* if(me_proc == 0){ */
  /*   printf("Check ranks %d, list", nprocs_inter); */
  /*   for(proc=0; proc<nprocs_inter; proc++) printf(" %d", list_ranks[proc]); */
  /*   printf("\n"); */
  /*   fflush(stdout); */
  /* } */
  MPI_Barrier(whole_comm);
      
  if(MPI_Comm_group(whole_comm, &grp_whole) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_group\n");
    MPI_Abort(MCW, -1);
  }
  if(MPI_Group_incl(grp_whole, nprocs_inter, list_ranks, &grp_inter) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_incl\n");
    MPI_Abort(MCW, -1);
  }
  if(MPI_Comm_create(whole_comm, grp_inter, inter_comm) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_create\n");
    MPI_Abort(MCW, -1);
  }

  if(me_proc_intra != 0) *inter_comm = MPI_COMM_SELF; //If process is not a member of inter_comm,
  MPI_Barrier(whole_comm);

  return dcb_status;
}

int separate_proc_socket(int **list_loads, int *init_socket_id, MPI_Comm const intra_comm, MPI_Comm *bound_comm){
  int i, j, id_socket, id_core, id_cpu, proc, sum_loads, temp_load, num_valid_cpus;
  int *list_loads_back, *array_proc, *num_valid_cpus_socket;
  float threashold;
  float *list_loads_sort;
  int color, key;
  int me_proc_intra, nprocs_intra, nprocs_bound;
  // int MPI_err;
  
  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_rank(intra_comm, &me_proc_intra);

  list_loads_sort = (float *)malloc(nprocs_intra * sizeof(float));
  array_proc = (int *)malloc(nprocs_intra * sizeof(int));
  sum_loads = 0;
  for(proc=0; proc < nprocs_intra; proc++){
    list_loads_sort[proc] = (*list_loads)[proc];
    array_proc[proc] = proc;
    sum_loads += (*list_loads)[proc];
  }
  quicksort(list_loads_sort, array_proc, nprocs_intra); //Gathering all balance and sorting them.

  num_valid_cpus_socket = (int *)malloc(info_cpu.num_sockets * sizeof(int)); //Counting valid cpus
  for(id_socket=0; id_socket<info_cpu.num_sockets; id_socket++){
    num_valid_cpus_socket[id_socket] = 0;
    for(j=0; j<info_cpu.max_cores_socket; j++){
      id_core = info_cpu.ptr_socket_to_core[id_socket][j];
      for(i=0; i<info_cpu.logic_HT; i++){
	id_cpu = info_cpu.ptr_core_to_cpu[id_core][i];
	if(info_bind.valid_cpu[id_cpu]) num_valid_cpus_socket[id_socket]++;
      }
    }
  }
#ifdef _verbose_debug
  if(me_proc_intra == 0){
    char *strg_list;
    int digit_cpuid = calc_digit(info_cpu.num_cpus);
    if((strg_list = (char *)malloc(sizeof(char) * (info_cpu.num_sockets * (digit_cpuid+2)))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }
    strg_list[info_cpu.num_sockets * (digit_cpuid+2) - 2] = '\0';
    sprintf(&strg_list[0], "%*d", digit_cpuid, num_valid_cpus_socket[0]);
    for(i=1; i<info_cpu.num_sockets; i++) //Check default affinity
      sprintf(&strg_list[i*(digit_cpuid+2)-2], ", %*d", digit_cpuid, num_valid_cpus_socket[i]);
    printf("DCB debug : Valid number of CPUs on each socket = %s\n", strg_list);
    free(strg_list);
  }
  MPI_Barrier(intra_comm);
#endif

  
  // color = 0;
  id_socket = 0;
  threashold = (float)sum_loads / (float)info_bind.num_cpus * (float)num_valid_cpus_socket[id_socket]; 
  temp_load = 0;
  i = 0;
  j = nprocs_intra-1;
  while(1){ //Loop until all cores are bound to any process.

    if(id_socket+1 == info_cpu.num_sockets) break;
    
    temp_load += list_loads_sort[i];
    if(array_proc[i] == me_proc_intra){
      // color = id_socket;
      break;
    }
    i++;
    if(temp_load >= threashold || threashold - temp_load < (temp_load + list_loads_sort[j]) - threashold){
      id_socket++;
      threashold += (float)sum_loads / (float)info_bind.num_cpus * (float)num_valid_cpus_socket[id_socket];
      temp_load = 0;
    }
      
    if(id_socket+1 == info_cpu.num_sockets) break;
    
    temp_load += list_loads_sort[j];
    if(array_proc[j] == me_proc_intra){
      // color = id_socket;
      break;
    }
    j--;
    if(temp_load >= threashold || threashold - temp_load < (temp_load + list_loads_sort[i]) - threashold){
      id_socket++;
      threashold += (float)sum_loads / (float)info_bind.num_cpus * (float)num_valid_cpus_socket[id_socket];
      temp_load = 0;
    }
      
    if(id_socket+1 == info_cpu.num_sockets) break;  
  }
  // if(color == 0) color = id_socket;
  color = id_socket;
  // *min_cpuid = info_cpu.ptr_core_to_cpu[info_cpu.ptr_socket_to_core[id_socket][0]][0];
  *init_socket_id = id_socket;
  // printf("Check me_proc_intra = %d, color = %d, init_socket_id = %d\n", me_proc_intra, color, *init_socket_id);
  
#ifdef _verbose_debug
  int *colors;
  if(me_proc_intra == 0){
    int digit_socketid = calc_digit(info_cpu.num_sockets);
    int digit_procid = calc_digit(nprocs_intra);
    char *strg_list, char_backup;
    int *ptr_strg1, *ptr_strg2;
    
    if((colors = (int *)malloc(sizeof(int) * nprocs_intra)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }
    MPI_Gather(&color, 1, MPI_INT, colors, 1, MPI_INT, 0, intra_comm);

    if((strg_list = (char *)malloc(sizeof(char) * (info_cpu.num_sockets * (digit_socketid+11) + nprocs_intra * (digit_procid+2)-2))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }

    for(i=0; i<info_cpu.num_sockets * (digit_socketid+11) + nprocs_intra * (digit_procid+2) - 2; i++)
      strg_list[i] = '*';
    strg_list[info_cpu.num_sockets * (digit_socketid+11) + nprocs_intra * (digit_procid+2) - 3] = '\0';

    if((ptr_strg1 = (int *)malloc(sizeof(int) * (info_cpu.num_sockets+1))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }
    if((ptr_strg2 = (int *)malloc(sizeof(int) * (info_cpu.num_sockets+1))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }

    ptr_strg1[0]  = 0;
    for(id_socket=0; id_socket<info_cpu.num_sockets; id_socket++){
      ptr_strg1[id_socket+1] = (id_socket + 1) * (digit_socketid + 10) - 1;
    }

    for(proc=0; proc<nprocs_intra; proc++){
      id_socket = colors[proc];
      ptr_strg1[id_socket+1] += (digit_procid + 2);
    }
    for(id_socket=0; id_socket<=info_cpu.num_sockets; id_socket++){
      ptr_strg2[id_socket] = ptr_strg1[id_socket];
      ptr_strg1[id_socket] += digit_socketid + 10;
    }
    ptr_strg1[0] -= 1;

    for(proc=0; proc<nprocs_intra; proc++){
      id_socket = colors[proc];
      char_backup = strg_list[ptr_strg1[id_socket]+digit_procid+2];
      sprintf(&strg_list[ptr_strg1[id_socket]], ", %*d", digit_procid, proc);
      strg_list[ptr_strg1[id_socket]+digit_procid+2] = char_backup;
      ptr_strg1[id_socket] += digit_procid + 2;
    }
    char_backup = strg_list[11+digit_socketid];
    sprintf(&strg_list[0], " Socket %*d = ", digit_socketid, 0);
    strg_list[11+digit_socketid] = char_backup;
    for(id_socket=1; id_socket<info_cpu.num_sockets; id_socket++){
      char_backup = strg_list[ptr_strg2[id_socket]+12+digit_socketid];
      sprintf(&strg_list[ptr_strg2[id_socket]], ", Socket %*d = ", digit_procid, id_socket);
      strg_list[ptr_strg2[id_socket]+12+digit_socketid] = char_backup;
    }
    strg_list[info_cpu.num_sockets * (digit_socketid+11) + nprocs_intra * (digit_procid+2)-3] = '\0';

    int l_cname;
    char *nodename;
    nodename = (char *)malloc(MPI_MAX_PROCESSOR_NAME * sizeof(char));
    if(MPI_Get_processor_name((char *)nodename, &l_cname) != MPI_SUCCESS){
      fprintf(stderr, "DCB : Failed MPI_Get_processor_name\n");
      MPI_Abort(MCW, -1);
    }
    printf("DCB debug : Processes in the node %s on%s\n", nodename, strg_list);
    free(colors);
    free(strg_list);
    free(ptr_strg1);
    free(ptr_strg2);
  }else{
    colors = NULL;
    MPI_Gather(&color, 1, MPI_INT, colors, 1, MPI_INT, 0, intra_comm);
  }
  MPI_Barrier(intra_comm);
#endif

  key = me_proc_intra;
  if(MPI_Comm_split(intra_comm, color, key, bound_comm) != MPI_SUCCESS){ //Create communicator for intra-socket
    fprintf(stderr, "DCB : Failed MPI_Comm_split\n");
    MPI_Abort(MCW, -1);
  }
  MPI_Comm_size(intra_comm, &nprocs_bound);

  list_loads_back = *list_loads;
  if((*list_loads = (int *)malloc(sizeof(int) * nprocs_bound)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(whole_comm, -1);
  }
  MPI_Allgather(&list_loads_back[me_proc_intra], 1, MPI_INT, *list_loads, 1, MPI_INT, *bound_comm);
  
  num_valid_cpus = num_valid_cpus_socket[color];
  
  free(list_loads_back);
  free(list_loads_sort);
  free(num_valid_cpus_socket);

  return num_valid_cpus;
}

int bind_socket(int **list_loads, int *init_socket_id, MPI_Comm const intra_comm, MPI_Comm *bound_comm){
  int color, key;
  int *list_loads_back;
  int me_proc_intra, nprocs_intra, nprocs_bound;
  
  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_rank(intra_comm, &me_proc_intra);

  color = floor((float)me_proc_intra / (float)info_cpu.num_sockets);
  key = me_proc_intra;
  // printf("Check me_proc = %d, color = %d\n", me_proc_intra, color);
  
  if(MPI_Comm_split(intra_comm, color, key, bound_comm) != MPI_SUCCESS){ //Create communicator for intra-socket
    fprintf(stderr, "DCB : Failed MPI_Comm_split\n");
    MPI_Abort(MCW, -1);
  }

  if(list_loads != NULL){
    MPI_Comm_size(*bound_comm, &nprocs_bound);
    list_loads_back = *list_loads;
    if((*list_loads = (int *)malloc(sizeof(int) * nprocs_bound)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }
    MPI_Allgather(&list_loads_back[me_proc_intra], 1, MPI_INT, *list_loads, 1, MPI_INT, *bound_comm);
  }
  
  // *min_cpuid = info_cpu.ptr_core_to_cpu[info_cpu.ptr_socket_to_core[color][0]][0];
  *init_socket_id = color;
  return info_cpu.max_cores_socket * info_bind.num_cpus_per_core;
}

int create_color_id(MPI_Comm const whole_comm) //Return color id
{
  int i, ptr;
  int l_cname; //, max_cname;
  // long int lint_temp;
  int color;
  long int l_color;
  // int *nodename_chars, *nodename_base;
  int num_ints, num_chars;
  int me_proc, num_procs;
  char *nodename, *nodename_ints;
  // char *endptr;

  MPI_Comm_rank(whole_comm, &me_proc);
  MPI_Comm_size(whole_comm, &num_procs);
  
  nodename = (char *)malloc(MPI_MAX_PROCESSOR_NAME * sizeof(char));
  if(MPI_Get_processor_name((char *)nodename, &l_cname) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_incl\n");
    MPI_Abort(MCW, -1);
  }

  /* MPI_Allreduce(&l_cname, &max_cname, 1, MPI_INT, MPI_MAX, whole_comm); */

  num_ints = 0;
  num_chars = 0;
  for(i=0; i<l_cname; i++){
    if('0' <= nodename[i] && nodename[i] <= '9'){
      num_ints++;
    }else{
      num_chars++;
    }
  }

  if(num_ints > calc_digit(INT_MAX)){
    printf("DCB : Internal error. Does not support more than %d values of nodename.\n", INT_MAX);
    printf("DCB : Aborting.\n");
    MPI_Abort(whole_comm, -1);
  }

  nodename_ints = (char *)malloc(l_cname * sizeof(char));
  ptr = 0;
  for(i=0; i<l_cname; i++){
    if('0' <= nodename[i] && nodename[i] <= '9'){
      nodename_ints[ptr] = nodename[i];
      ptr++;
    }
  }
  nodename_ints[ptr] = '\0';  
  
  /* nodename_chars = (int *)malloc(max_cname * sizeof(int)); */
  /* lint_temp = 0; */
  /* num_chars = 0; */
  /* j = 0; */
  /* for(i=0; i<l_cname; i++){ */
  /*   if('0' <= nodename[i] && nodename[i] <= '9'){ */
  /*     lint_temp += ((int)nodename[i]-(int)'0')*(int)pow(10, num_ints-j-1); */
  /*     j++; */
  /*   }else{ */
  /*     nodename_chars[num_chars] = (int)nodename[i]; */
  /*     num_chars++; */
  /*   } */
  /* } */
  /* color = (int)lint_temp; */

  // l_color = strtoul(nodename, &endptr, 0);
  // l_color = atoi(nodename);
  l_color = 0;
  sscanf(nodename_ints, "%ld", &l_color);
  // printf("Check color = %ld, nodename = %s, endptr = %s\n", l_color, nodename, endptr);

  /* if(num_ints == 0 && num_procs != 1){ */
  /*   printf("DCB : Does not support node names that have non-numeric differences.\n"); */
  /*   printf("DCB : This version only supports node name as fooXXXbar. (XXX denotes numerals)\n"); */
  /*   printf("DCB : Aborting.\n"); */
  /*   MPI_Abort(whole_comm, -1); */
  /* } */

  if(l_color >= INT_MAX){
    printf("DCB : Does not support more than %d included node ID.\n", INT_MAX);
    printf("DCB : Aborting.\n");
    MPI_Abort(whole_comm, -1);
  }

  color = l_color;
  
#ifdef _verbose_debug
  // printf("DCB_debug : Calculated color id : nodename = %s, color_id = %d, num_ints = %d, num_chars = %d\n", nodename, color, num_ints, num_chars);
  printf("DCB_debug : Calculated color id : nodename = %s, color_id = %d\n", nodename, color);
#endif
  
  /* for(i=num_chars; i<max_cname; i++) */
  /*   nodename_chars[i] = CHAR_MAX+1; */

  /* if(lint_temp > (long int)INT_MAX){ */
  /*   printf("DCB : Internal error. Does not support more than %d values of nodename.\n", INT_MAX); */
  /*   printf("DCB : Aborting.\n"); */
  /*   MPI_Abort(whole_comm, -1); */
  /* } */

  /* nodename_base = (int *)malloc(max_cname * sizeof(int)); */
  /* MPI_Allreduce(nodename_chars, nodename_base, max_cname, MPI_INT, MPI_MIN, whole_comm); */
  /* for(i=0; i<max_cname; i++){ */
  /*   if(nodename_chars[i] != nodename_base[i]) break; */
  /* } */

  /* if(i != max_cname){ */
  /*   printf("DCB : Still does not support node names that have non-numeric differences.\n"); */
  /*   printf("DCB : This version only supports node name as fooXXXbar. (XXX denotes numerals)\n"); */
  /*   printf("DCB : Aborting.\n"); */
  /*   MPI_Abort(whole_comm, -1); */
  /* } */

  /* long int test; */
  /* test = 0; */
  /* pat_ascii = pow(2, 8*sizeof(char)-1); */
  /* for(i=0; i<(int)pow(2, 8*(sizeof(int)-sizeof(char)))-1; i++) */
  /*   test += pat_ascii*i; */
  /*     printf("Check ascii = %d, characters = %d, bit = %d, sum = %ld\n", pat_ascii, (int)pow(2, 8*(sizeof(int)-sizeof(char)))-1, \ */
  /* 	     8*(int)(sizeof(int)-sizeof(char)), test); */
  /* if(id_max-id_min+1 > (int)pow(2, 8*(sizeof(int)-sizeof(char))-1)){ */
  /*   if(me_proc == 1){ */
  /*     printf("DCB : Internal error, Difference of names among nodes are big difference.\n"); */
  /*     printf("DCB : This library only supports different node names with less than %d consecutive characters.\n", \ */
  /*                                                                                                   (int)pow(2, 8*(sizeof(int)-sizeof(char))-1)); */
  /*     MPI_Abort(whole_comm, -1); */
  /*   } */
  /* }					    */
  /* MPI_Barrier(whole_comm); */
  
  /* pat_ascii = pow(2, 8*sizeof(char)-1); */
  /* color = 0; */
  /* for(i=id_min; i<id_max; i++){ */
  /*   color += (nodename_own[i]-nodename_base[i])+pat_ascii*(int)pow(2, (id_max-i-1)); */
  /* } */

#ifdef _verbose_debug
  printf("DCB debug : Check nodename = %s and color = %d\n", (char *)nodename, color);
#endif

  return color;

}

int create_valid_table(MPI_Comm const intra_comm, MPI_Comm const inter_comm){
  int i, j, id_core, id_cpu, id_hc, count_valid_core;
  int nprocs_intra;
  int me_proc_intra, me_proc_inter;

  if(info_bind.flag_alloc == 1) return dcb_success;

  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_rank(intra_comm, &me_proc_intra);
  MPI_Comm_rank(inter_comm, &me_proc_inter);
  info_bind.flag_alloc = 1;

  if((info_bind.valid_cpu = (int *)malloc(sizeof(int) * info_cpu.num_cpus)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  
  if(info_bind.num_excl_cpus != 0 || info_bind.num_excl_cores != 0|| info_bind.num_cores != 0){

    if(info_bind.num_cores*info_bind.num_cpus_per_core > info_cpu.num_cpus_online){
      fprintf(stderr, "DCB : Exported number of cores and number of cpus per core is larger than number of online CPUs\n");
      fprintf(stderr, "DCB : Please check \"DCB_NUM_CORES\" and \"DCB_NUM_CPUS_PER_CORE\"\n");
      return dcb_threadmap_failure;
    }

    int ptr = 0;
    for(id_cpu=0; id_cpu<info_cpu.num_cpus; id_cpu++) //Initialize info_bind.valid_cpu
      if(info_cpu.list_cpus_online[ptr] == id_cpu){
	// if(me_proc_intra == 1) printf("valid %d, %d\n", info_cpu.list_cpus_online[ptr], id_cpu);
	info_bind.valid_cpu[id_cpu] = 1;
	ptr++;
      }else{
	// if(me_proc_intra == 1) printf("invalid %d, %d\n", info_cpu.list_cpus_online[ptr], id_cpu);
	info_bind.valid_cpu[id_cpu] = 0;
      }
    
    for(i=0; i<info_bind.num_excl_cpus; i++)
      info_bind.valid_cpu[info_bind.excl_list_cpu[i]] = 0; //Disabling excluded cpus

    for(i=0; i<info_bind.num_excl_cores; i++){
      id_core = info_bind.excl_list_core[i];
      for(j=0; j<info_cpu.logic_HT; j++){
	id_cpu = info_cpu.ptr_core_to_cpu[id_core][j];
	info_bind.valid_cpu[id_cpu] = 0; //Disabling excluded cores
      }
    }

  }else{
    for(i=0; i<info_cpu.num_cpus; i++)
      info_bind.valid_cpu[i] = info_cpu.mapped_list[i];
  }
  
  if(info_bind.num_cpus_per_core > 0){
    for(id_core=0; id_core<info_cpu.num_cores; id_core++)
      for(i=info_bind.num_cpus_per_core; i<info_cpu.logic_HT; i++){
  	id_cpu = info_cpu.ptr_core_to_cpu[id_core][i];
  	info_bind.valid_cpu[id_cpu] = 0; //Disabling CPUs more than exported number of CPUs per core.
      }
  }
  
  if(info_bind.num_cores > 0){
    count_valid_core = 0;
    for(id_core=0; id_core<info_cpu.num_cores; id_core++)
      for(i=0; i<info_bind.num_cpus_per_core; i++){
	id_cpu = info_cpu.ptr_core_to_cpu[id_core][i];
	if(count_valid_core > info_bind.num_cores){
	  info_bind.valid_cpu[id_cpu] = 0; //Disabling cores more than exported number of cores.
	}else if(info_bind.valid_cpu[id_cpu]){
	  count_valid_core++;
	  break;
	}
      }

    if(count_valid_core < info_bind.num_cores){
      if(me_proc_intra == 0 && me_proc_inter == 0){
	fprintf(stderr, "DCB : There is a mismatch among exported environment values\n");
	fprintf(stderr, "DCB : DCB_NUM_CORES is larger than the number of valid cores set by DCB_LIST_CPUIDS_EXCLUDING and DCB_LIST_COREIDS_EXCLUDING\n");
	fprintf(stderr, "DCB : Please check theses environment values\n");
	fprintf(stderr, "DCB : \"DCB_VERBOSE=1\" outputs all environment values \n");
      }
      return dcb_threadmap_failure;
    }
  }else{
    info_bind.num_cores = 0;
    for(id_core=0; id_core<info_cpu.num_cores; id_core++)
      for(i=0; i<info_bind.num_cpus_per_core; i++){
	id_cpu = info_cpu.ptr_core_to_cpu[id_core][i];
	if(info_bind.valid_cpu[id_cpu]){
	  info_bind.num_cores++;
	  break;
	}
      }
  }

  info_bind.num_cpus = 0;
  for(id_core=0; id_core<info_cpu.num_cores; id_core++)
    for(id_hc=0; id_hc<info_bind.num_cpus_per_core; id_hc++){
      id_cpu = info_cpu.ptr_core_to_cpu[id_core][id_hc];
      if(info_bind.valid_cpu[id_cpu]){
	info_bind.num_cpus++;
	info_bind.last_valid_cpu = id_cpu; // For binding core to process which has small load.
      }
    }
  

  if(info_bind.num_cpus == 0){
    if(me_proc_intra == 0){
      if(me_proc_inter == 0){
	fprintf(stderr, "DCB : There is no valid CPU because of excluding CPUs by the environment values\n");
	fprintf(stderr, "DCB : Please check exported environment values\n");
	fprintf(stderr, "DCB : \"DCB_VERBOSE=1\" outputs all environment values \n");
      }
    }
    return dcb_threadmap_failure;
  }

  // info_bind.num_thrds = info_bind.num_thrds_per_cpu * info_bind.num_cpus;

  if(flag_verbose && me_proc_intra == 0)
    if(me_proc_inter == 0){
      printf("DCB : Number of valid cores = %d\n", info_bind.num_cores);
      printf("DCB : Number of valid CPUs = %d list = ", info_bind.num_cpus);
      for(i=0; i<info_cpu.num_cpus; i++){
	if(info_bind.valid_cpu[i]){
	  printf("T, ");
	}else{
	  printf("F, ");	  
	}
      }
      printf("\n");
      // printf("DCB : Number of total threads = %d\n", info_bind.num_thrds);
  }
  fflush(stdout);
  
  return dcb_success;
}

int balancing_core(int const load, MPI_Comm const intra_comm, MPI_Comm const inter_comm){
  int bcpus, num_max_thrds, err;
  int *reserve_list;
  int me_proc;
  
  MPI_Comm_rank(intra_comm, &me_proc);

  if(load < 0){
    if(me_proc == 0) printf("DCB : dcb_balance failed, invalid load value. Load value must be positive value. ");
    return dcb_invalid_argument;
  }
  
#ifdef _measure_time
  count_reserve++;
  time_reserve = time_reserve - omp_get_wtime();
#endif

  if(info_bind.mode_pr == 1){ //Power aware mode

    // err = reserve_cores(1, &bcpus, &num_max_thrds, &reserve_list, intra_comm, inter_comm, whole_comm);
    if((reserve_list = (int *)malloc(sizeof(int) * presets[0].num_cpus)) == NULL){ //copy reserve list of initial condition
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    for(int i=0; i<presets[0].num_cpus; i++)
      reserve_list[i] = presets[0].reserve_list[i];
    bcpus = presets[0].num_cpus;
    err = reserve_cores_pa(load, &bcpus, &num_max_thrds, &reserve_list, intra_comm, inter_comm, whole_comm);

  }else if(info_bind.mode_pr == 2){ //Focusing on memory mode

    // err = reserve_cores(1, &bcpus, &num_max_thrds, &reserve_list, intra_comm, inter_comm, whole_comm);
    if((reserve_list = (int *)malloc(sizeof(int) * presets[0].num_cpus)) == NULL){ //copy reserve list of initial condition
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    for(int i=0; i<presets[0].num_cpus; i++)
      reserve_list[i] = presets[0].reserve_list[i];
    bcpus = presets[0].num_cpus;
    err = reserve_cores_mem(load, &bcpus, &num_max_thrds, &reserve_list, intra_comm, inter_comm, whole_comm);

  }else if(info_bind.mode_pr == 3){ //Hybrid mode (speed and PA)
    printf("Check hybrid mode\n");

    err = reserve_cores(load, &bcpus, &num_max_thrds, &reserve_list, intra_comm, inter_comm, whole_comm);
    err = reserve_cores_pa(load, &bcpus, &num_max_thrds, &reserve_list, intra_comm, inter_comm, whole_comm);

  }else{ //Reducing computational time mode

    err = reserve_cores(load, &bcpus, &num_max_thrds, &reserve_list, intra_comm, inter_comm, whole_comm);

  }

  fflush(stdout);
#ifdef _measure_time
  time_reserve = time_reserve + omp_get_wtime();
  count_bind++;
  time_bind = time_bind - omp_get_wtime();
#endif

  err = bind_threads_to_cpu(bcpus, num_max_thrds, reserve_list, intra_comm);

#ifdef _measure_time
  time_bind = time_bind + omp_get_wtime();
#endif
  
  return err;
}

// load : Loads of each process
// bcpus : number of cpus bound to process
// num_max_thrds : number of threads
// reserve_list : mask for determining reserving cpus
int reserve_cores(int const load, int *bcpus, int *num_max_thrds, int **reserve_list, MPI_Comm const intra_comm, MPI_Comm const inter_comm, MPI_Comm const whole_comm){
  int i, id_core, id_cpu, id_socket, id_bcpu, bcpus_min, bcpus_sum, bcpus_rem, bcpus_attach, flag_zero_bcpus, count_cpus;
  int num_bound_cpus, init_socket_id;
  int *list_loads, *array_proc, *array_bcpus;
  long int sum_loads;
  float bcpus_real, balance;
  float *array_balance;
  int proc, me_proc_intra, me_proc_inter, nprocs_intra, flag_bound_comm;
  MPI_Comm bound_comm;
  int me_proc_bound, nprocs_bound;
  MPI_Status recv_st;
  
  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_rank(intra_comm, &me_proc_intra);
  MPI_Comm_rank(inter_comm, &me_proc_inter);

  fflush(stdout);
  
  if((list_loads = (int *)malloc(sizeof(int) * nprocs_intra)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }

  MPI_Allgather(&load, 1, MPI_INT, list_loads, 1, MPI_INT, intra_comm);

  if(info_bind.bind_mode == 1){ //A process is not mapped to different sockets (Close on each socket. Processes don't move among sockets)
    if(me_proc_intra == 0) printf("Mode = socket\n");
    num_bound_cpus = bind_socket(&list_loads, &init_socket_id, intra_comm, &bound_comm);
    flag_bound_comm = 1;
  }else if(info_bind.flag_proc_diffsockets == 0){ //A process is not mapped to different sockets.
    num_bound_cpus = separate_proc_socket(&list_loads, &init_socket_id, intra_comm, &bound_comm);
    flag_bound_comm = 1;
  }else{ //There is a case that a process is mapped to multi sockets.
    bound_comm = intra_comm;
    num_bound_cpus = info_bind.num_cpus;
    init_socket_id = 0;
    flag_bound_comm = 0;
  }
  MPI_Comm_size(bound_comm, &nprocs_bound);
  MPI_Comm_rank(bound_comm, &me_proc_bound);
  //num_bound_cpus : Number of enable cpus
  // printf("Check num_bound_cpus = %d, me_proc_intra = %d, me_proc_bound = %d\n", num_bound_cpus, me_proc_intra, me_proc_bound);
  
  sum_loads = 0;
  for(proc=0; proc<nprocs_bound; proc++){
    sum_loads += list_loads[proc];
  }

  if(!info_bind.flag_diffprocs_core){ //If each process has an independent core, we bind one core to each process, firstly.
    *bcpus = info_bind.num_cpus_per_core;
    // bcpus_rem = num_bound_cpus - nprocs_bound * info_bind.num_cpus_per_core; //And we calculate number of CPUs based on the number of physical cores,
  }else{                                            //, multiply the bcpus with number of number of hardware contexts.
    * bcpus = 0;
    // bcpus_rem = num_bound_cpus;
  }
  if(info_bind.flag_diffprocs_core){
    bcpus_attach = 1;
  }else{
    bcpus_attach = info_bind.num_cpus_per_core; //Same as number of hardware context on core.
  }
  //bcpus_rem    : number of remained enable cpus
  //bcpus_attach : number of attached cpus per core

  fflush(stdout);

  MPI_Allreduce(bcpus, &bcpus_sum, 1, MPI_INT, MPI_SUM, bound_comm);
  bcpus_rem = num_bound_cpus - bcpus_sum; //Calc remained cpus that is not mapped any process
  // printf("Check bcpus_rem = %d, bcpus_sum = %d\n", bcpus_rem, bcpus_sum);

  // printf("Check0 int me_proc = %d, bcpus = %d, num_bound_cpus = %d\n", me_proc_intra, *bcpus, num_bound_cpus);

  //Calculating number of bounding CPUs
  // printf("Check bef bcpus = %d\n", *bcpus);
  // bcpus_real = ((float)num_bound_cpus / (float)bcpus_attach) * ((float)load / (float)sum_loads) - *bcpus / bcpus_attach;
  bcpus_real = ((float)bcpus_rem / (float)bcpus_attach) * ((float)load / (float)sum_loads);
  if(bcpus_real < 0) bcpus_real = 0.0;
  // printf("Check1 bcpus_real = %f, bcpus_rem = %d, bcpus_attach = %d, load = %d, sum_loads = %ld\n", bcpus_real, bcpus_rem,
  //	 bcpus_attach, load, sum_loads);
  //If HT is enabled && flag_diffprocs_corc(not bound different processy on core : bpucs_attach = number of HT, else : bcpus_attach = 1)
  *bcpus += (int)floor((double)bcpus_real) * bcpus_attach;
  // printf("Check2 bcpus, adding = %d, %d, %d\n", (int)floor((double)bcpus_real), bcpus_attach, *bcpus);
  balance = (float)load / (float)sum_loads * (float)num_bound_cpus - (float)*bcpus;
  //balance : load-imbalance with bcpus
  
  // printf("Check3 conditions = %d, %d, and num_bound_cpus = %d\n", info_bind.bind_mode, info_bind.flag_proc_diffsockets, num_bound_cpus);
  // printf("Check4 bcpus_real = %f, bcpu_attach = %d\n", bcpus_real, bcpus_attach);
  // printf("Check5 balance = %f, load = %d, sum_load = %ld, sum_bound_cpus = %d, bcpus = %d\n", balance, load, sum_loads, num_bound_cpus, *bcpus);
  
  MPI_Allreduce(bcpus, &bcpus_sum, 1, MPI_INT, MPI_SUM, bound_comm);
  bcpus_rem = num_bound_cpus - bcpus_sum; //Calc remained cpus that is not mapped any process
  // printf("Check6 bcpus_rem = %d, num_bound_cpu = %d, bcpus_sum = %d\n", bcpus_rem, num_bound_cpus, bcpus_sum);
  if(bcpus_rem < 0){
    //printf("Check6 bcpus_rem = %d, num_bound_cpu = %d, bcpus_sum = %d\n", bcpus_rem, num_bound_cpus, bcpus_sum);
    fprintf(stderr, "DCB : Internal error\n. DCB : bcpus_rem has negative value.\nAorting\n");
    fflush(stdout);
    fflush(stderr);
    MPI_Abort(MPI_COMM_WORLD, -1);
  }

  // printf("Check1 int me_proc = %d, bcpus = %d, bcpus_real = %f, balance = %f, bcpus_rem = %d, num_bound_cpus = %d\n", me_proc_intra, *bcpus, bcpus_real, balance, bcpus_rem, num_bound_cpus);

  // printf("Check1 me_proc_intra = %d, me_proc_bound = %d, bcpu = %d, bcpus_rem = %d\n", me_proc_intra, me_proc_bound, *bcpus, bcpus_rem);

  //If there are un-mapped CPUs or more than num_bound_cpus CPUs are mapped,
  //Tuning mapping based on "balance" value
  if(bcpus_rem != 0){
/* #ifdef _verbose_debug */
/*     if(me_proc_bound == 0) */
/*       printf("DCB debug : Some CPUs are not bound to any process based on the passed load, Number of CPUs on system = %d, Bound number of CPUs = %d, Remain = %d\n", \ */
/* 	     info_bind.num_cpus, bcpus_sum, bcpus_rem); */
/* #endif */
    if((array_balance = (float *)calloc(nprocs_bound, sizeof(float))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    MPI_Allgather(&balance, 1, MPI_FLOAT, array_balance, 1, MPI_FLOAT, bound_comm);
    if((array_proc = (int *)malloc(sizeof(int) * nprocs_bound)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    for(proc=0; proc < nprocs_bound; proc++)
      array_proc[proc] = proc;
    quicksort(array_balance, array_proc, nprocs_bound); //Gathering all balance and sorting them.
    /* if(me_proc_bound == 0) */
    /*   for(proc=0; proc<nprocs_bound; proc++) */
    /* 	printf("Check result of sort, array_balance = %f, array_proc = %d\n", array_balance[proc], array_proc[proc]); */

    if(bcpus_rem > 0){
      for(i=0; i<nprocs_bound; i++)
	if(array_proc[i] == me_proc_bound && i+1 <= bcpus_rem/bcpus_attach)
	  (*bcpus) += bcpus_attach; //Increasing bound cores in the order of balance_rem.
    }/* else{ //bcpus_rem < 0 */
    /*   for(i=nprocs_bound-1; i>=0; i--){ */
    /* 	printf("Check branch me_proc = %d, array_proc = %d, left = %d, right = %d\n", me_proc_bound, array_proc[i], (nprocs_bound-i), -1*bcpus_rem/bcpus_attach); */
    /* 	if(array_proc[i] == me_proc_bound && nprocs_bound-i <= -1*bcpus_rem/bcpus_attach){ */
    /* 	  printf("Hit reducing %d, %d\n", me_proc_bound, i); */
    /* 	  (*bcpus) -= bcpus_attach; //Increasing bound cores in the order of balance_rem. */
    /* 	} */
    /*   } */
    /* } */
    
    free(array_balance);
    free(array_proc);
  }
  // printf("Check2 int me_proc = %d, bcpus = %d, balance = %f, bcpus_rem = %d, num_bound_cpus = %d\n", me_proc_intra, *bcpus, balance, bcpus_rem, num_bound_cpus);
  
  MPI_Allreduce(bcpus, &bcpus_min, 1, MPI_INT, MPI_MIN, bound_comm);
  if(bcpus_min == 0){ //If there is a bcpus which is zero
    balance = bcpus_real - *bcpus;

    if((array_balance = (float *)calloc(nprocs_bound, sizeof(float))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    MPI_Allgather(&balance, 1, MPI_FLOAT, array_balance, 1, MPI_FLOAT, bound_comm);    
    if((array_proc = (int *)malloc(sizeof(int) * nprocs_bound)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(MCW, -1);
    }
    for(proc=0; proc < nprocs_bound; proc++)
      array_proc[proc] = proc;
    quicksort(array_balance, array_proc, nprocs_bound);

    if(me_proc_bound == array_proc[nprocs_bound-1]) (*bcpus) -= bcpus_attach; //Decreasing number of bound cpus from process which has smallest balance (the negative value). 

    flag_zero_bcpus = 1;
    
    free(array_balance);
    free(array_proc);
  }else{
    flag_zero_bcpus = 0;
  }
  
  if((array_bcpus = (int *)malloc(sizeof(int) * nprocs_bound)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }
  MPI_Allgather(bcpus, 1, MPI_INT, array_bcpus, 1, MPI_INT, bound_comm);    
  MPI_Allreduce(bcpus, &bcpus_sum, 1, MPI_INT, MPI_SUM, bound_comm);

  // printf("Check me_proc_intra = %d, me_proc_bound = %d, bcpu = %d, bcpus_sum = %d, info_bind.num_cpus = %d\n", me_proc_intra, me_proc_bound, *bcpus, bcpus_sum, info_bind.num_cpus);
  MPI_Barrier(whole_comm);
  if(bcpus_sum != num_bound_cpus-flag_zero_bcpus){
    if(me_proc_bound == 0) fprintf(stderr, "DCB : Internal error. All CPUs are not mapped.\n");
    if(me_proc_bound == 0) fprintf(stderr, "DCB : Sum of bound CPUs = %d, number of valid CPUs = %d.\n",
				   bcpus_sum, num_bound_cpus);
    MPI_Abort(MCW, -1);
  }

  if(*bcpus % info_bind.num_cpus_per_core != 0 && !info_bind.flag_diffprocs_core){
    fprintf(stderr, "DCB : Internal error. Different processes are bound on same core\n");
    // MPI_Abort(MCW, -1);    
  }

  if(*bcpus == 0){
    *bcpus = 1;
    *num_max_thrds = 1;
  }else{
    *num_max_thrds = *bcpus * info_bind.num_thrds_per_cpu;
  }
  
  if((*reserve_list = (int *)malloc(sizeof(int) * *bcpus)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(MCW, -1);
  }

  /* offset_cpuid = flag_zero_found; */
  /* for(proc=0; proc<me_proc_bound; proc++) */
  /*   offset_cpuid += array_bcpus[proc]; */

  if(me_proc_bound != 0){
    MPI_Recv(&id_socket, 1, MPI_INT, me_proc_bound-1, 0, bound_comm, &recv_st);
    MPI_Recv(&id_core,   1, MPI_INT, me_proc_bound-1, 0, bound_comm, &recv_st);
    MPI_Recv(&id_cpu,    1, MPI_INT, me_proc_bound-1, 0, bound_comm, &recv_st);
  }else{
    id_socket = init_socket_id;
    id_core = 0;
    id_cpu = 0;      
  }
  // printf("Check initial ids, me_proc = %d, id_core = %d, id_cpu = %d\n", me_proc_bound, id_core, id_hc);

  if(array_bcpus[me_proc_bound] == 0){
    (*reserve_list)[0] = info_bind.last_valid_cpu;
  }else{ //Reserve cores by a process (a start core id is passed by a process which have lower process ID.)
    count_cpus = 0;
    while(count_cpus < *bcpus){
      
      // printf("Check me_proc_bound = %d, count_cpus = %d, id_socket = %d, id_core = %d, id_cpu = %d, count_cpus = %d, *bcpus = %d\n", me_proc_bound, count_cpus, id_socket, id_core, id_cpu, count_cpus, *bcpus);
      id_bcpu = info_cpu.ptr_core_to_cpu[info_cpu.ptr_socket_to_core[id_socket][id_core]][id_cpu];
      // id_cpu = info_cpu.ptr_core_to_cpu[id_core][id_hc];

      //If CPU is valid,
      if(info_bind.valid_cpu[id_bcpu]){
	(*reserve_list)[count_cpus] = id_bcpu;
	count_cpus++;
      }

      id_cpu++;
      if(id_cpu == info_bind.num_cpus_per_core){
	id_core++;
	id_cpu = 0;
      }
      if(id_core == info_cpu.num_cores_socket[id_socket]){
	id_socket++;
	id_core = 0;
	id_cpu = 0;
      }
    }
  }

  if(me_proc_bound != nprocs_bound-1){
    // printf("Sending info src = %d, tgt = %d, id_socket = %d, id_core = %d, id_cpu = %d\n", me_proc_bound, me_proc_bound+1, id_socket, id_core, id_cpu);
    MPI_Send(&id_socket, 1, MPI_INT, me_proc_bound+1, 0, bound_comm);
    MPI_Send(&id_core,   1, MPI_INT, me_proc_bound+1, 0, bound_comm);
    MPI_Send(&id_cpu,	 1, MPI_INT, me_proc_bound+1, 0, bound_comm);
  }
  
#ifdef _verbose_debug
  char *strg_list;
  int digit_cpuid = calc_digit(info_cpu.num_cpus);
  if((strg_list = (char *)malloc(sizeof(char) * (*bcpus * (digit_cpuid+2)))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(whole_comm, -1);
  }
  strg_list[*bcpus * (digit_cpuid+2) - 2] = '\0';
  sprintf(&strg_list[0], "%*d", digit_cpuid, (*reserve_list)[0]);
  for(i=1; i<*bcpus; i++) //Check default affinity
    sprintf(&strg_list[i*(digit_cpuid+2)-2], ", %*d", digit_cpuid, (*reserve_list)[i]);
  printf("DCB debug : Reserved CPUs init to process %d, number of CPUs = %d, list = %s\n", me_proc_intra, *bcpus, strg_list);
  free(strg_list);
#endif
  
  free(array_bcpus);
  if(flag_bound_comm) MPI_Comm_free(&bound_comm);
  
  return dcb_success;
}

int reserve_cores_pa(int const load, int *bcpus, int *num_max_thrds, int **reserve_list, MPI_Comm const intra_comm, MPI_Comm const inter_comm, MPI_Comm const whole_comm){
  int nprocs_intra, me_proc_intra;
  // int *list_loads, *list_num_cpus;
  int max_load_core, bcpus_original;
  int load_core;
    
  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_rank(intra_comm, &me_proc_intra);

  /* MPI_Comm_size(whole_comm, nprocs_whole); */
  /* MPI_Comm_rank(whole_cimm, me_proc_whole); */

  
  /* if(me_proc_whole == 0){ */
  /*   if((list_loads = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){ */
  /*     fprintf(stderr, "DCB : Memory allocation failed\n"); */
  /*     MPI_Abort(MCW, -1); */
  /*   } */
  /*   if((num_cpus = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){ */
  /*     fprintf(stderr, "DCB : Memory allocation failed\n"); */
  /*     MPI_Abort(MCW, -1); */
  /*   } */
  /* } */

  /* MPI_Gather(&load, 1, MPI_INT, list_loads, 1, MPI_INT, 0, intra_comm); */
  /* MPI_Gather(&num_cores, 1, MPI_INT, list_loads, 1, MPI_INT, 0, intra_comm); */

  load_core = load / *bcpus;
  
  MPI_Allreduce(&load_core, &max_load_core, 1, MPI_INT, MPI_MAX, whole_comm); //Allreduce with whole processes

  bcpus_original = *bcpus;
  *bcpus = ceil((float)load_core / (float)max_load_core * (*bcpus));
  *bcpus += *bcpus%info_bind.num_cpus_per_core; //Care for HT
  if(*bcpus > bcpus_original) *bcpus  = bcpus_original; //Because of arithmetic err, there is a case that this condition is met.
  if(*bcpus < info_bind.min_cpus) *bcpus = info_bind.min_cpus;
  *num_max_thrds = *bcpus * info_bind.num_thrds_per_cpu;

#ifdef _verbose_debug
  char *strg_list;
  int digit_cpuid = calc_digit(info_cpu.num_cpus);
  if((strg_list = (char *)malloc(sizeof(char) * (*bcpus * (digit_cpuid+2)))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(whole_comm, -1);
  }
  strg_list[*bcpus * (digit_cpuid+2) - 2] = '\0';
  sprintf(&strg_list[0], "%*d", digit_cpuid, (*reserve_list)[0]);
  for(int i=1; i<*bcpus; i++) //Check default affinity
    sprintf(&strg_list[i*(digit_cpuid+2)-2], ", %*d", digit_cpuid, (*reserve_list)[i]);
  printf("DCB debug : Reserved CPUs for PA to process %d, number of CPUs = %d, list = %s\n", me_proc_intra, *bcpus, strg_list);
  free(strg_list);
#endif
  
  return dcb_success;
}

int reserve_cores_mem(int const load, int *bcpus, int *num_max_thrds, int **reserve_list, MPI_Comm const intra_comm, MPI_Comm const inter_comm, MPI_Comm const whole_comm){
  int nprocs_intra, me_proc_intra;
  // int *list_loads, *list_num_cpus;
  int max_load, bcpus_original;
    
  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_rank(intra_comm, &me_proc_intra);

  /* MPI_Comm_size(whole_comm, nprocs_whole); */
  /* MPI_Comm_rank(whole_cimm, me_proc_whole); */

  
  /* if(me_proc_whole == 0){ */
  /*   if((list_loads = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){ */
  /*     fprintf(stderr, "DCB : Memory allocation failed\n"); */
  /*     MPI_Abort(MCW, -1); */
  /*   } */
  /*   if((num_cpus = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){ */
  /*     fprintf(stderr, "DCB : Memory allocation failed\n"); */
  /*     MPI_Abort(MCW, -1); */
  /*   } */
  /* } */

  /* MPI_Gather(&load, 1, MPI_INT, list_loads, 1, MPI_INT, 0, intra_comm); */
  /* MPI_Gather(&num_cores, 1, MPI_INT, list_loads, 1, MPI_INT, 0, intra_comm); */

  MPI_Allreduce(&load, &max_load, 1, MPI_INT, MPI_MAX, intra_comm); //Allreduce in each nodes

  bcpus_original = *bcpus;
  *bcpus = ceil((float)load / (float)max_load * (*bcpus));
  *bcpus += *bcpus%info_bind.num_cpus_per_core; //Care for HT
  if(*bcpus > bcpus_original) *bcpus  = bcpus_original; //Because of arithmetic err, there is a case that this condition is met.
  if(*bcpus < info_bind.min_cpus) *bcpus = info_bind.min_cpus;
  *num_max_thrds = *bcpus * info_bind.num_thrds_per_cpu;
  
#ifdef _verbose_debug
  char *strg_list;
  int digit_cpuid = calc_digit(info_cpu.num_cpus);
  if((strg_list = (char *)malloc(sizeof(char) * (*bcpus * (digit_cpuid+2)))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(whole_comm, -1);
  }
  strg_list[*bcpus * (digit_cpuid+2) - 2] = '\0';
  sprintf(&strg_list[0], "%*d", digit_cpuid, (*reserve_list)[0]);
  for(int i=1; i<*bcpus; i++) //Check default affinity
    sprintf(&strg_list[i*(digit_cpuid+2)-2], ", %*d", digit_cpuid, (*reserve_list)[i]);
  printf("DCB debug : Reserved CPUs for PA to process %d, number of CPUs = %d, list = %s\n", me_proc_intra, *bcpus, strg_list);
  free(strg_list);
#endif
  
  return dcb_success;
}

int bind_threads_to_cpu(int num_cpus, int num_max_thrds, int *reserve_list, MPI_Comm const intra_comm){
  int me_thrd, id_reserve_list, me_proc;
  int err, err_syscall;
  size_t size_mask;
  pid_t pid;
  cpu_set_t *mask_affinity;
  
  err = dcb_success;
  MPI_Comm_rank(whole_comm, &me_proc);

  omp_set_num_threads(num_max_thrds);
  //Calling sched_setaffinity
#pragma omp parallel default(none) \
  shared(stderr, info_cpu, info_bind, reserve_list, err) \
  private(me_thrd, mask_affinity, err_syscall, size_mask, pid, id_reserve_list) \
  firstprivate(num_cpus, me_proc)
  {
    me_thrd = omp_get_thread_num();
    id_reserve_list = floor((float)me_thrd / (float)info_bind.num_thrds_per_cpu);
    
    mask_affinity = CPU_ALLOC(info_cpu.num_cpus);
    size_mask = CPU_ALLOC_SIZE(info_cpu.num_cpus);
    CPU_ZERO_S(size_mask, mask_affinity);
    err_syscall = CPU_SET_S(reserve_list[id_reserve_list], size_mask, mask_affinity);

    pid = gettid();
    err_syscall = sched_setaffinity(pid, size_mask, mask_affinity);
    if (err_syscall != 0){
      fprintf(stderr, "DCB : sched_set_affinity_error on proc %d\n", me_proc);
#pragma omp atomic write
      err = dcb_threadmap_failure;
    }
    CPU_FREE(mask_affinity);

    // printf("Process %d, thread %d is bound to core %d\n", me_proc, me_thrd, me_proc*num_cpus+me_thrd);
  }

  if(err == dcb_threadmap_failure){
    // MPI_Barrier(whole_comm);
    MPI_Abort(whole_comm, -1);
  }
  
  return err;
}
