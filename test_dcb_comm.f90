program main
  use mpi
  use omp_lib
  use dcb
  implicit none
  integer load
  integer me_proc, me_proc_new, MPI_ierr, err
  integer provided
  integer :: required = MPI_THREAD_SERIALIZED
  integer MCW_NEW
  integer, parameter :: num_sockets=2, num_drms=2
  integer :: pos_socket(num_sockets) = (/ 12, 13 /), pos_drm(num_drms) = (/ 10, 11 /)
 
  call MPI_Init_thread(required, provided, mpi_ierr)
  call MPI_Comm_rank(MPI_COMM_WORLD, me_proc, mpi_ierr)
  me_proc = me_proc + 1
  if(provided < required) then
     if(me_proc == 0) write(*, *) "Requred mpi operation type is not supported\n"
     call MPI_Abort(MPI_COMM_WORLD, -1, mpi_ierr)
  endif
     
  call MPI_Barrier(MPI_COMM_WORLD, mpi_ierr)

  load = 10 * me_proc

  ! write(*, *) 'Check comm init', me_proc, MPI_COMM_WORLD
  
  call dcb_output_param_f(load, "balance_nodes.txt", MPI_COMM_WORLD, err)
  call MPI_Barrier(MPI_COMM_WORLD, mpi_ierr)
  ! call MPI_Abort(MPI_COMM_WORLD, -1, MPI_ierr)

  ! write(*, *) 'Check bef create new_comm' 
  call dcb_create_new_comm_f("test_neworder.txt", MPI_COMM_WORLD, MCW_NEW, err)
  call MPI_Comm_rank(MCW_NEW, me_proc_new, mpi_ierr)
  me_proc_new = me_proc_new + 1
  write(*, *) "Comp rank", me_proc, me_proc_new
  
  call MPI_Finalize(MPI_ierr)
  
end program main
  
