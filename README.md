# DCB library

DCB is a library for dinamically controlling number of cores bound to process.


## Requirement

"[lscpu](https://man7.org/linux/man-pages/man1/lscpu.1.html)" command is required.
Please check a target system has "lscpu" command.

## Compile

Modify header of Makefile for compiling library.

```bash
make #Compile library
make fortran_interface #Compile fortran interface
make test_c #Compile C sample program
make test_f #Compile Fortran sample program
```

## Usage

Please check Readme.pptx

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
