# SYSTEM := INTEL
SYSTEM := GNU
# SYSTEM := INTELdebug
# SYSTEM := GNUdebug
# SYSTEM := PGI
# SYSTEM := A64fx
# SYSTEM := A64fx_debug

#lib_private:=/home/naosou/original_env
path_powerstat := /home/naosou/original_env/temp/powerstat
power_api := on
include_path := /opt/FJSVtcs/pwrm/aarch64/include
link_path := /opt/FJSVtcs/pwrm/aarch64/lib64

#intel
ifeq ($(SYSTEM),INTEL)
CC:=mpiicc
FC:=mpiifort
OPTFLAGS := -O2
LDFLAGS := -std=gnu11 -qopenmp -mkl -D_measure_time -D_path_powerstat=\"$(path_powerstat)\"
CFLAGS := $(OPTFLAGS) $(LDFLAGS)
FFLAGS := -fpp -stand f03 -qopenmp -diag-disable 5268
LINKC := xiar cr
endif

#intel
ifeq ($(SYSTEM),INTELdebug)
CC:=mpiicc
FC:=mpiifort
DFLAGS := -O0 -g -traceback -debug full
LDFLAGS := -std=gnu11 -qopenmp -mkl -D_verbose_debug -D_path_powerstat=\"$(path_powerstat)\"
CFLAGS := $(DFLAGS) $(LDFLAGS)
FFLAGS := -fpp -stand f03 -qopenmp -diag-disable 5268
LINKC := xiar cr
endif

#GNU
ifeq ($(SYSTEM),GNU)
CC:=mpicc
FC:=mpif90
OPTFLAGS:= -O2
LDFLAGS:= -std=gnu11 -fopenmp -lm -D_path_powerstat=\"$(path_powerstat)\"
# LDFLAGS:= -std=gnu11 -fopenmp -lm
CFLAGS := $(OPTFLAGS) $(LDFLAGS)
FFLAGS := -cpp -std=f2003 -fopenmp -ffree-line-length-none
OPTS_LINK_F := -fall-intrinsics
LINKC := ar cr
endif

#GNU_debug
ifeq ($(SYSTEM),GNUdebug)
CC:=mpicc
FC:=mpif90
DFLAGS:= -O0 -g -Wall
LDFLAGS:= -std=gnu11 -fopenmp -lm -D_verbose_debug -D_measure_time -D_path_powerstat=\"$(path_powerstat)\"
CFLAGS := $(DFLAGS) $(LDFLAGS)
FFLAGS := -cpp -std=f2003 -fopenmp -g -fbacktrace -ffree-line-length-none
OPTS_LINK_F := -fall-intrinsics
LINKC := ar cr
endif

#PGI
ifeq ($(SYSTEM),PGI)
CC:=mpicc
FC:=mpif90
OPTFLAGS:= -O2
LDFLAGS:= -c11 -mp
CFLAGS := $(OPTFLAGS) $(LDFLAGS)
FFLAGS := -Mpreprocess -mp
LINKC := ar cr
endif

#A64fx
ifeq ($(SYSTEM),A64fx)
CC:=mpifccpx
FC:=mpifrtpx
OPTFLAGS := -O2 -Kfast,openmp
LDFLAGS := -D_measure_time
ifeq ($(power_api),on)
LDFLAGS += -D_enable_PowerAPI -I$(include_path) -L$(link_path) -lpwr
flag_powerapi := true
endif
CFLAGS := $(OPTFLAGS) $(LDFLAGS) -std=gnu11
FFLAGS := $(OPTFLAGS) $(LDFLAGS) -Cpp -X03
LINKC := ar cr
endif

#A64fx_debug
ifeq ($(SYSTEM),A64fx_debug)
CC:=mpifccpx
FC:=mpifrtpx
OPTFLAGS := -O0 -Kopenmp -g
LDFLAGS := -D_verbose_debug -D_measure_time
ifeq ($(power_api),on)
LDFLAGS += -D_enable_PowerAPI -I$(include_path) -L$(link_path) -lpwr
flag_powerapi := true
endif
CFLAGS := $(OPTFLAGS) $(LDFLAGS) -std=gnu11
FFLAGS := $(OPTFLAGS) $(LDFLAGS) -Cpp -X03
LINKC := ar cr
endif

TARGET_C := libdcb.a
OBJECTS_C := dcb_core.o dcb_common.o dcb_opt_routines.o
ifeq ($(flag_powerapi),true)
OBJECTS_C += dcb_powerapi.o
endif

TARGET_F := libdcb_f.a
OBJECTS_F := mod_dcb.o

testcode_c := testprogram.c
test_fname_c := test_dcb_c

testcode_f := testprogram.f90
test_fname_f := test_dcb_f

src_create_hostfile := create_hostfile.o

${TARGET_C} : ${OBJECTS_C}
	${LINKC} $@ ${OBJECTS_C}

${TARGET_F} : ${OBJECTS_F}
	${LINKC} $@ ${OBJECTS_F}

.SUFFIXES :
.SUFFIXES :.c .f90 .o

.c.o :
	${CC} ${CFLAGS} -c $<

.f90.o :
	${FC} ${FFLAGS} -c $<

all : ${TARGET_C} ${TARGET_F}

fortran_interface : ${TARGET_F}

test_c :
	${CC} ${CFLAGS} ${testcode_c} ${TARGET_C} -o ${test_fname_c}

test_f :
	${FC} ${FFLAGS} ${OPTS_LINK_F} ${testcode_f} ${TARGET_F} ${TARGET_C} -o ${test_fname_f}

create_hostfile : ${src_create_hostfile}
	${CC} ${CFLAGS} $^ ${TARGET_C} -o $@

.PHONY: clean
clean : 
	rm -f ${OBJECTS_C} ${OBJECTS_F} ${INTERVAL_MOD}
