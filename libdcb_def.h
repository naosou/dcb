#define dcb_success 0
#define dcb_threadmap_failure -1
#define dcb_malloc_failure -2
#define dcb_get_info_failure -3
#define dcb_external_library_error -4
#define dcb_unsupport -5
#define dcb_uninitialized -6
#define dcb_invalid_argument -7
#define dcb_invalid_preset_no -8
#define dcb_power_measuring_failure -9
