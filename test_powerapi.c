#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "pwr.h"
#include "fj_tool/fapp.h"

int const size_array = 13;

int main()
{
  PWR_Cntxt cntxt = NULL;
  PWR_Obj objs[size_array];
  PWR_Grp grp = NULL;
  // PWR_AttrName attrs[7];
  int rc, rca[size_array];
  int numobjs;
  double energy1, energy2;
  double *energy_stat, *energy_end, *energy;
  double ave_power = 0.0;
  PWR_Time ts1, ts2;
  PWR_Time *ts_stat, *ts_end;
  PWR_Status st;
  PWR_TimePeriod *period;

  // Power APIのコンテキスト取得
  rc = PWR_CntxtInit(PWR_CNTXT_FX1000, PWR_ROLE_APP, "app", &cntxt);
  if (rc != PWR_RET_SUCCESS) {
    printf("CntxtInit Failed\n");
    return 1;
  }

  rc = PWR_GrpCreate(cntxt, &grp);
  if (rc != PWR_RET_SUCCESS) {
    printf("GrpCreate Failed\n");
    return 1;
  }

  rca[0]  = PWR_CntxtGetObjByName(cntxt, "plat.node",                  &objs[0]);
  rca[1]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg0.cores",   &objs[1]);
  rca[2]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg1.cores",   &objs[2]);
  rca[3]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg2.cores",   &objs[3]);
  rca[4]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg3.cores",   &objs[4]);
  rca[5]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg0.l2cache", &objs[5]);
  rca[6]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg1.l2cache", &objs[6]);
  rca[7]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg2.l2cache", &objs[7]);
  rca[8]  = PWR_CntxtGetObjByName(cntxt, "plat.node.cpu.cmg3.l2cache", &objs[8]);
  rca[9]  = PWR_CntxtGetObjByName(cntxt, "plat.node.mem0",             &objs[9]);
  rca[10] = PWR_CntxtGetObjByName(cntxt, "plat.node.mem1",             &objs[10]);
  rca[11] = PWR_CntxtGetObjByName(cntxt, "plat.node.mem2",             &objs[11]);
  rca[12] = PWR_CntxtGetObjByName(cntxt, "plat.node.mem3",             &objs[12]);
  for(int i = 0; i < size_array; i++){
    if(rca[i] != PWR_RET_SUCCESS){
      printf("DCB : Error with calling PWR_CntxtGetObjByName, ID = %d, rc = %d\n", i, rca[i]);
      return 1;
    }
  }

  rca[0]  = PWR_GrpAddObj(grp, objs[0]);
  rca[1]  = PWR_GrpAddObj(grp, objs[1]);
  rca[2]  = PWR_GrpAddObj(grp, objs[2]);
  rca[3]  = PWR_GrpAddObj(grp, objs[3]);
  rca[4]  = PWR_GrpAddObj(grp, objs[4]);
  rca[5]  = PWR_GrpAddObj(grp, objs[5]);
  rca[6]  = PWR_GrpAddObj(grp, objs[6]);
  rca[7]  = PWR_GrpAddObj(grp, objs[7]);
  rca[8]  = PWR_GrpAddObj(grp, objs[8]);
  rca[9]  = PWR_GrpAddObj(grp, objs[9]);
  rca[10] = PWR_GrpAddObj(grp, objs[10]);
  rca[11] = PWR_GrpAddObj(grp, objs[11]);
  rca[12] = PWR_GrpAddObj(grp, objs[12]);
  for(int i = 0; i < size_array; i++){
    if(rca[i] != PWR_RET_SUCCESS){
      printf("DCB : Error with calling PWR_GrpAddObj, ID = %d, rc = %d\n", i, rca[i]);
      return 1;
    }
  }
  numobjs = PWR_GrpGetNumObjs(grp);
  printf("Check numobjs in grp = %d\n", numobjs);

  rc = PWR_GrpCreateStat(grp, PWR_ATTR_POWER, PWR_ATTR_STAT_SUM, &st);
  if (rc != PWR_RET_SUCCESS) {
    printf("GrpCreateStat Failed\n");
    return 1;
  }
  
  energy_stat = malloc( numobjs * sizeof(double) );
  energy_end  = malloc( numobjs * sizeof(double) );
  ts_stat = malloc( numobjs * sizeof(PWR_Time) );
  ts_end  = malloc( numobjs * sizeof(PWR_Time) );
  energy = malloc( numobjs * sizeof(double) );
  period = malloc( numobjs * sizeof(PWR_TimePeriod) );

  // Objectの推定電力量を取得
  // fapp_start("point", 1, 0);
  rc = PWR_ObjAttrGetValue(objs[0], PWR_ATTR_MEASURED_ENERGY, &energy1, &ts1);
  if (rc != PWR_RET_SUCCESS) {
    printf("ObjAttrGetValue1 Failed (rc = %d)\n", rc);
    return 1;
  }
  rc = PWR_GrpAttrGetValue(grp, PWR_ATTR_ENERGY, energy_stat, ts_stat, NULL);
  if (rc != PWR_RET_SUCCESS) {
    printf("GrpAttrGetValue1 Failed (rc = %d)\n", rc);
    return 1;
  }

  rc = PWR_StatStart(st);
  
  sleep(3);
  
  rc = PWR_StatStop(st);
  
  rc = PWR_ObjAttrGetValue(objs[0], PWR_ATTR_MEASURED_ENERGY, &energy2, &ts2);
  if (rc != PWR_RET_SUCCESS) {
    printf("ObjAttrGetValue2 Failed (rc = %d)\n", rc);
    return 1;
  }
  rc = PWR_GrpAttrGetValue(grp, PWR_ATTR_ENERGY, energy_end, ts_end, NULL);
  if (rc != PWR_RET_SUCCESS) {
    printf("GrpAttrGetValue2 Failed (rc = %d)\n", rc);
    return 1;
  }
  /* rc = PWR_ObjAttrGetValues(objs, get_vals, attrs, energy_end, ts_end, NULL); */
  /* if (rc != PWR_RET_SUCCESS) { */
  /*   printf("ObjAttrGetValue Failed (rc = %d)\n", rc); */
  /*   return 1; */
  /* } */
  printf("Check3\n");
  // fapp_stop("point", 1, 0);

  // 2つの計測点の電力量から、平均電力を算出
  ave_power = (energy2 - energy1) / ((ts2 - ts1) / 1000000000.0);
  printf("Measured ave_power = %lf, diff = %lf\n", ave_power, energy2 - energy1);

  ave_power = (energy_end[0] - energy_stat[0]) / ((ts_end[0] - ts_stat[0]) / 1000000000.0);
  printf("Estimate ave_power = %lf, diff = %lf\n", ave_power, energy_end[0] - energy_stat[0]);

  for(int i = 0; i < numobjs; i++)
    printf("Check i = %d, energy = %f\n", i, energy_end[i]-energy_stat[i]);

  // 統計量取得
  rc = PWR_StatGetValues(st, energy, period);
  if (rc != PWR_RET_SUCCESS) {
    printf("StatGetValue Failed (rc = %d)\n", rc);
    return 1;
  }
  for(int i = 0; i < numobjs; i++)
    printf("Check i = %d, energy = %f, periods = %lu, %lu, %lu\n", i, energy[i], period[i].start, period[i].stop, period[i].instant);

  // 統計Object破棄
  PWR_StatDestroy(st);

  // コンテキスト破棄
  PWR_CntxtDestroy(cntxt);
  
  return 0;
}
