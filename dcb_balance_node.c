#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <scip/scip.h>
#include <scip/scipdefplugins.h>

// #define ppn 4
#define max_line 1000

SCIP_RETCODE dcb_balance_MIP(int const nprocs, int const nnodes, int const ppn, int *loads, int (*mapping)[nnodes])
{
  int i, j;
  // int nnodes = 2, nprocs = 4;
  SCIP_Real *loads_scip;
  char name[SCIP_MAXSTRLEN];
  FILE *fp;

  if((loads_scip = (SCIP_Real*)malloc(nprocs * sizeof(SCIP_Real))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  for(i=0;i<nprocs;i++)
    loads_scip[i] = (SCIP_Real)loads[i];
  
  printf("Check1\n");

  SCIP* scip;
  SCIP_CALL( SCIPcreate(&scip) );
  
  SCIP_CALL( SCIPincludeDefaultPlugins(scip) );
  
  SCIP_CALL( SCIPsetRealParam(scip, "limits/gap", 0.01) );
  SCIP_CALL( SCIPsetIntParam(scip, "parallel/maxnthreads", 32) );
  // SCIP_CALL( SCIPwriteParams(scip, "scipmip.set", TRUE, FALSE) );

  SCIP_CALL( SCIPcreateProbBasic(scip, "Balance_node"));
    
  // SCIP_CALL(SCIPsetObjsense(scip, SCIP_OBJSENSE_MAXIMIZE));
  printf("Check2\n");

  //Define max_load
  SCIP_VAR* max_load;
  SCIP_CALL( SCIPcreateVarBasic(scip, &max_load, "max_load", 0.0, SCIPinfinity(scip), 1.0, SCIP_VARTYPE_INTEGER));
  SCIP_CALL( SCIPaddVar(scip, max_load) );

  // SCIP_VAR* (*map_procs)[nprocs];
  // if((*map_procs = (SCIP_VAR*[nprocs])malloc(nnodes * nprocs * sizeof(SCIP_VAR*))) == NULL){
  //   fprintf(stderr, "DCB : Memory allocation failed\n");
  //   exit(1);
  // }
  SCIP_VAR* **map_procs;
  if((map_procs = (SCIP_VAR***)malloc(nprocs * sizeof(SCIP_VAR**))) == NULL){
       fprintf(stderr, "DCB : Memory allocation failed\n");
       exit(1);
  }
  for(i=0;i<nprocs;i++)
    if((map_procs[i] = (SCIP_VAR**)malloc(nnodes * sizeof(SCIP_VAR*))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      exit(1);
    }
  // SCIP_VAR* map_procs[nprocs][nnodes];

  //Define nodemaps
  for(j=0;j<nnodes;j++)
    for(i=0;i<nprocs;i++){
      // printf("Check map_procs, i=%i, j=%i\n", i, j);
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "node_%i_proc_%i", j, i);
      SCIP_CALL( SCIPcreateVarBasic(scip, &(map_procs[i][j]), name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY) );
      SCIP_CALL( SCIPaddVar(scip, map_procs[i][j]) );
    }
  printf("Check3\n");

  /* if ((fp = fopen("Check_stage.txt", "w")) == NULL) { */
  /*   printf("file open error!!\n"); */
  /*   exit(EXIT_FAILURE); */
  /* } */
  /* SCIP_CALL( SCIPprintStage(scip, fp) ); */
  /* fclose(fp); */

  /* if ((fp = fopen("Check_status.txt", "w")) == NULL) { */
  /*   printf("file open error!!\n"); */
  /*   exit(EXIT_FAILURE); */
  /* } */
  /* SCIP_CALL( SCIPprintStatus(scip, fp) ); */
  /* fclose(fp); */
  
  SCIP_CONS* *balance;
  if((balance = (SCIP_CONS**)malloc(nnodes * sizeof(SCIP_CONS*))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  // Define equations for minimize load an-balance among nodes
  // SCIP_CONS* balance[nnodes];
  // printf("Check3.5\n");
  for(j=0;j<nnodes;j++){
    (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "node_%i", j);
    SCIP_CALL(SCIPcreateConsBasicLinear(scip, &balance[j], name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0));
    // printf("Check3.6 %i\n", j);
    SCIP_CALL(SCIPaddCoefLinear(scip, balance[j], max_load, -1.0));
    // printf("Check3.7 %i\n", j);
    for(i=0;i<nprocs;i++){
      // printf("Check3.8 %i, %i\n", j, i);
      SCIP_CALL(SCIPaddCoefLinear(scip, balance[j], map_procs[i][j], loads_scip[i]));
      // printf("Check3.81 %i, %i\n", j, i);
    }
    SCIP_CALL(SCIPaddCons(scip, balance[j]));
    // printf("Check3.9 %i\n", j);
  }
  printf("Check4\n");

  SCIP_CONS* *part;
  if((part = (SCIP_CONS**)malloc(nprocs * sizeof(SCIP_CONS*))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  // SCIP_CONS* part[nprocs];
  //Define constructs that all process mapped any node.
  for(i=0;i<nprocs;i++){
    (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "part_%i", i);
    SCIP_CALL(SCIPcreateConsBasicSetpart(scip, &part[i], name, nnodes, map_procs[i]));
    SCIP_CALL(SCIPaddCons(scip, part[i]));
  }
  printf("Check5\n");


#ifdef const_ppn
  SCIP_CONS* *bind_ppn;
  if((bind_ppn = (SCIP_CONS**)malloc(nnodes * sizeof(SCIP_CONS*))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  //Define number of process par node.
  for(j=0;j<nnodes;j++){
    (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "bind_ppn_%i", j);
    SCIP_CALL(SCIPcreateConsBasicLinear(scip, &bind_ppn[j], name, 0, NULL, NULL, (double)ppn, (double)ppn));
    for(i=0;i<nprocs;i++){
      // printf("Check3.8 %i, %i\n", j, i);
      SCIP_CALL(SCIPaddCoefLinear(scip, bind_ppn[j], map_procs[i][j], 1.0));
      // printf("Check3.81 %i, %i\n", j, i);
    }
    SCIP_CALL(SCIPaddCons(scip, bind_ppn[j]));
    // printf("Check3.9 %i\n", j);
  }
#endif
  
  printf("Check6\n");

  for(j=0;j<nnodes;j++)
    SCIP_CALL( SCIPreleaseCons(scip, &balance[j]) );
  for(i=0;i<nprocs;i++)
    SCIP_CALL( SCIPreleaseCons(scip, &part[i]) );
#ifdef const_ppn
  for(j=0;j<nnodes;j++)
    SCIP_CALL( SCIPreleaseCons(scip, &bind_ppn[j]) );
#endif
  printf("Check7\n");

  // SCIP_CALL( SCIPsolve(scip) );
  // SCIP_CALL( SCIPsolveParallel(scip) );
  SCIP_CALL( SCIPsolveConcurrent(scip) );
  SCIP_SOL* sol;
  sol = SCIPgetBestSol(scip);
  printf("Check8\n");

  SCIP_CALL( (SCIPwriteOrigProblem(scip, "dcb_balance.lp", NULL, FALSE)));
  SCIP_CALL( (SCIPwriteTransProblem(scip, "dcb_balance_trans.lp", NULL, FALSE)));

  // SCIP_Real result[nprocs][nnodes];
  SCIP_Real (*result)[nnodes];
  if((result = (SCIP_Real(*)[nnodes])malloc(nnodes * nprocs * sizeof(SCIP_VAR*))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }  
  for(i=0;i<nprocs;i++)
     SCIP_CALL( SCIPgetSolVals(scip, sol, nnodes, map_procs[i], result[i]) );
  /* for(i=0;i<nprocs;i++) */
  /*   for(j=0;j<nnodes;j++) */
  /*     result[i][j] = (int)SCIPgetSolVal(scip, sol, map_procs[i][j]); */
  
  for(j=0;j<nnodes;j++){
    printf("Check result node=%i, proc=", j);
    for(i=0;i<nprocs;i++){
      printf("%i,", (int)result[i][j]);
      mapping[i][j]=(int)result[i][j];
    }
    printf("\n");
  }
  
  SCIP_CALL( SCIPreleaseVar(scip, &max_load) );
  for(j=0;j<nnodes;j++)
    for(i=0;i<nprocs;i++)
      SCIP_CALL( SCIPreleaseVar(scip, &(map_procs[i][j])) );
  
  SCIP_CALL( SCIPfree(&scip));
  
        
  return SCIP_OKAY;
    
}

int GetRandom(int min,int max)
{
  return min + (int)(rand()*(max-min+1.0)/(1.0+RAND_MAX));
}

int main(int argc, const char *argv[]) {
  int i, j, proc, load;
  int nnodes, nprocs, ppn;
  int *loads;
  int idum1, idum2;
  int len_input, len_output;
  // long int load_read;
  int load_read;
  SCIP_RETCODE ret_balance;
  char *fname_input, *fname_output;
  char line[max_line];
  double ddum;
  FILE *fp;
  
#ifdef test //For test (create random number)
#ifdef const_ppn
  if(argc != 4){
    printf("Wrong arguments %i\n", argc);
    exit(1);
  }
#else
  if(argc != 3){
    printf("Wrong arguments %i\n", argc);
    exit(1);
  }
#endif
  sscanf(argv[1], "%i", &nprocs);
  sscanf(argv[2], "%i", &nnodes);
#ifdef const_ppn
  sscanf(argv[3], "%i", &ppn);
#endif
  if((loads = (int *)malloc(nprocs * sizeof(int))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  for(i=0;i<nprocs;i++){
    loads[i] = GetRandom(10, 100);
  }
  printf("\n");
  
#else //For reading file
#ifdef const_ppn
  if(argc != 3){
    printf("Wrong arguments %i\n", argc);
    exit(1);
  }
#else
  if(argc != 2){
    printf("Wrong arguments %i\n", argc);
    exit(1);
  }
#endif
  // fname = argv[1];
#ifdef const_ppn
  sscanf(argv[2], "%i", &ppn);
#endif
  len_input = strlen(argv[1]);
  if((fname_input = (char *)malloc((len_input+1) * sizeof(char))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  strcpy(fname_input, argv[1]);
  printf("Check fname = %s\n", fname_input);
  if ((fp = fopen(fname_input, "r")) == NULL) {
    printf("file open error!!\n");
    exit(EXIT_FAILURE);
  }
  fscanf(fp, "%i %i", &nprocs, &nnodes);
  printf("Check reading data nprocs = %i, nnodes = %i\n", nprocs, nnodes);
  if((loads = (int *)malloc(nprocs * sizeof(int))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  for(proc=0; proc<nprocs; proc++){
    printf("Proc=%i\n", proc);
    // fscanf(fp, "%i, %i", &proc, &load);
    // loads[proc] = load;
    loads[proc] = 0;
    while(1){
      fgets(line, max_line, fp);
      printf("Check read line : %s\n", line);
      // printf("Check head of read line : %i, %i, %i\n", line[0], line[1], line[2]);
      if(strcmp(line, " \n") == 0) break;
      sscanf(line, "%i %i %le %i", &idum1, &idum2, &ddum, &load_read);
      printf("Check load_read = %i\n", load_read);
      if((long int)loads[proc] + load_read > INT_MAX){
	printf("Sum of loads is larger than INT_MAX\nAborting\n");
	exit(1);
      }
      loads[proc] += load_read;
    }
  }
  fclose(fp);
#endif

  printf("Check number of procs = %i, number of nodes = %i\n", nprocs, nnodes);
  printf("Check loads=");
  for(i=0;i<nprocs;i++){
    printf("%i, ", loads[i]);
  }
  printf("\n");
  
  int (*mapping)[nnodes];
  if((mapping = (int (*)[nnodes])malloc(nnodes * nprocs * sizeof(int*))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }

  ret_balance = dcb_balance_MIP(nprocs, nnodes, ppn, loads, mapping);

  len_output = len_input + 8;
  if((fname_output = (char *)malloc((len_output+1) * sizeof(char))) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    exit(1);
  }
  for(i=len_input; i>=0; i--){
    if(fname_input[i] == '.'){
      break;
    }
  }
  // memcpy(fname_output, fname_input, i-1);
  strcpy(fname_output, fname_input);
  // printf("Check fname_output1 = %s\n", fname_output);
  memcpy(&fname_output[i], "_balance", 8);
  memcpy(&fname_output[i+8], &fname_input[i], len_input-i+1);
  fname_output[len_output+1] = '\0';
  printf("Check fname_output = %s\n", fname_output);
  if ((fp = fopen(fname_output, "w")) == NULL) {
    printf("file open error!!\n");
    exit(EXIT_FAILURE);
  }
  for(j=0; j<nnodes; j++){
    fprintf(fp, "%d ", j);
    for(i=0; i<nprocs; i++)
      if(mapping[i][j]) fprintf(fp, "%d ", i);
    fprintf(fp, "\n");
  }
  fclose(fp);
  
  return ret_balance != SCIP_OKAY ? 1 : 0;
    
}
