#include <mpi.h>

#ifdef _enable_PowerAPI
#include "dcb_papi.h"
#define flag_papi 1
#else
#define flag_papi 0
#endif

#ifdef _path_powerstat
#define flag_pmstat 1
#else
#define flag_pmstat 0
#endif

int extract_int(char const *str_in, int **int_out);
int ascending_order(const void *a, const void *b);
int create_color_id(MPI_Comm const whole_comm);
int create_intra_comm(MPI_Comm const whole_comm, MPI_Comm *intra_comm, MPI_Comm *inter_comm);
int calc_digit(int val);
int calc_binding_cores(int load, int sum_loads, MPI_Comm intra_comm);
void quicksort(float *master, int *slave, int n);
void quicksort_int(int *master, int *slave, int n);
void *remalloc(void *old_array, int const dsize, int const old_size, int const new_size);
void finalize_newcomm_incproc();
