#ifndef _LIBDCB_SUB_H
#define _LIBDCB_SUB_H     1

#define balance_success 0
#define balance_failure -1
#define bad_range -2
#define bad_threshold -3

#define balance_greedy 0
#define balance_fullsearch 1
#define balance_one_swap 2

#define unit_malloc 1000

#define threshold_count_loop 10

extern int method_balance, range_candidate, flag_verbose_balance;
extern double threshold_BandB, threshold_greedy, threshold_cand, threshold_cand_mat;

// int balance_node(int *node_send, int *node_recv, int const load, MPI_Comm const whole_comm, MPI_Comm const inter_comm, MPI_Comm const intra_comm);
int balance_node(int *node_send, int *node_recv, int *list_loads, int const n, MPI_Comm const whole_comm, MPI_Comm const inter_comm, MPI_Comm const intra_comm);

#endif
