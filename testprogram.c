#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <omp.h>
#include "libdcb.h"

int main(int argc, char* argv[])
{
  int load;
  int num_procs, me_proc, MPI_err, err;
  int provided;
  int required=MPI_THREAD_SERIALIZED;
 
  MPI_err = MPI_Init_thread(&argc, &argv, required, &provided);
  MPI_err = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI_err = MPI_Comm_rank(MPI_COMM_WORLD, &me_proc);
  if (provided < required) {
    if(me_proc == 0) printf("Requred mpi operation type is not supported\n");
    MPI_err = MPI_Barrier(MPI_COMM_WORLD);
  }
  
  load = 10 * (me_proc + 1);
  // if(me_proc == 0) load = 1;
  // if(me_proc == 1) load = 3;

  printf("Check number of processes = %d, threads = %d, passed paramter = %d\n", num_procs, omp_get_max_threads(), load);
  MPI_err = MPI_Barrier(MPI_COMM_WORLD);

  err = dcb_init(MPI_COMM_WORLD);

  //Set presets for lower latency 
  // err = dcb_set_preset(1, 1);
  // err = dcb_set_preset(2, load);

  err = dcb_verbose("Before balance");
  err = dcb_balance(load);
  err = dcb_verbose("After balance");

  // err = dcb_balance_preset(1);
  // err = dcb_verbose("Even balance");

  // err = dcb_balance_preset(2);
  // err = dcb_verbose("Re-balancing with preset");
  
#pragma omp parallel
  {
    err = dcb_verbose("After balance in parallel");    
  }
  
  err = dcb_free();
  
  err = MPI_Finalize();
  return err;
}
