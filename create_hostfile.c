#include "dcb_intra.h"

int main(int argc, char* argv[])
{
  int me_proc_whole, nprocs_whole, me_proc_intra, nprocs_intra, me_proc_inter, nprocs_inter;
  int l_cname, err;
  int provided;
  int required=MPI_THREAD_SERIALIZED;
  char *fname;
  char *nodename;
  char (*list_nodenames)[MPI_MAX_PROCESSOR_NAME];
  FILE *fp;
  MPI_Comm intra_comm, inter_comm;
 
  MPI_Init_thread(&argc, &argv, required, &provided);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs_whole);
  MPI_Comm_rank(MPI_COMM_WORLD, &me_proc_whole);  
  if (provided < required) {
    if(me_proc_whole == 0) printf("Requred mpi operation type is not supported\n");
    err = MPI_Barrier(MPI_COMM_WORLD);
  }

  intra_comm = MPI_COMM_NULL;
  inter_comm = MPI_COMM_NULL;
  //Create communicator among intra-processes(inside of a node) and inter-processes
  err = create_intra_comm(MPI_COMM_WORLD, &intra_comm, &inter_comm);
  
  MPI_Comm_size(intra_comm, &nprocs_intra);
  MPI_Comm_size(inter_comm, &nprocs_inter);
  MPI_Comm_rank(intra_comm, &me_proc_intra);
  MPI_Comm_rank(inter_comm, &me_proc_inter);

  if(me_proc_intra == 0){
    nodename = (char *)malloc(MPI_MAX_PROCESSOR_NAME * sizeof(char));
    if(MPI_Get_processor_name((char *)nodename, &l_cname) != MPI_SUCCESS){
      fprintf(stderr, "DCB : Failed MPI_Comm_incl\n");
      MPI_Abort(MCW, -1);
    }

    if(me_proc_inter == 0){
      list_nodenames=(char(*)[MPI_MAX_PROCESSOR_NAME])malloc(nprocs_inter*MPI_MAX_PROCESSOR_NAME*sizeof(char));
    }
    MPI_Gather(nodename, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, list_nodenames, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, inter_comm);

    if(me_proc_inter == 0){
      if ((fp = fopen(argv[1], "r")) == NULL) {
	printf("File open failed.\n");
	printf("Aborting\n");
	exit(EXIT_FAILURE);
      }
      int *num_procs_node;
      num_procs_node = (int *)calloc(nprocs_inter, sizeof(int));
      int node, temp;
      for(int i = 0; i < nprocs_whole; i++){
	fscanf(fp, "%d %d\n", &node, &temp);
	num_procs_node[node]++;
      }
      fclose(fp);
      
      for(node = 0; node < nprocs_inter; node++)
	printf("Number of procs = %d on node = %d\n", num_procs_node[node], node);


      if ((fp = fopen(argv[2], "w")) == NULL) {
	printf("File open failed.\n");
	printf("Aborting\n");
	exit(EXIT_FAILURE);
      }
      for(node = 0; node < nprocs_inter; node++){
	if(strcmp(argv[3], "OpenMPI") == 0)
	  fprintf(fp, "%s slots=%d\n", list_nodenames[node], num_procs_node[node]);
	else if(strcmp(argv[3], "MPICH") == 0)
	  fprintf(fp, "%s:%d\n", list_nodenames[node], num_procs_node[node]);
      }
      fclose(fp);
    }
  }
  MPI_Finalize();
}
