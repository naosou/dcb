#!/bin/bash

est_time=$1
shift  # Skip the first argument (est_time)
list_cpu=("$@")

# Construct the CPU paths
cpu_paths=("/sys/devices/system/cpu/cpu${list_cpu[0]}/cpufreq/scaling_cur_freq")
echo -n ${list_cpu[0]}
for ((i = 1; i < ${#list_cpu[@]}; i++)); do
    cpu_paths+=("/sys/devices/system/cpu/cpu${list_cpu[i]}/cpufreq/scaling_cur_freq")
    echo -n ' '${list_cpu[i]}
done
echo

# Main loop
for ((i = 1; i <= est_time; i++)); do
    # Read frequencies for all CPUs in parallel and print them
    frequencies=($(cat "${cpu_paths[@]}" | tr '\n' ' '))
    # for ((j = 0; j < ${#list_cpu[@]}; j++)); do
    #     echo -n "CPU ${list_cpu[j]} Frequency: ${frequencies[j]} "
    # done
    echo ${frequencies[@]}
    
    # Sleep for 1 second before the next iteration
    sleep 1
done
