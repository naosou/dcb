#include <stdio.h>
#include <stdlib.h>

#include <mpi.h>

#include "dcb_balance.h"

#define num_ppn 4

int main(int argc, char* argv[])
{
  int i, n, send, recv;
  int rand_max = 1000, rand_min = 500;
  int *list_loads;
  int num_procs, me_proc, MPI_err, err;
  int provided;
  int required=MPI_THREAD_SERIALIZED;
 
  MPI_err = MPI_Init_thread(&argc, &argv, required, &provided);
  MPI_err = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI_err = MPI_Comm_rank(MPI_COMM_WORLD, &me_proc);
  if (provided < required) {
    if(me_proc == 0) printf("Requred mpi operation type is not supported\n");
    MPI_err = MPI_Barrier(MPI_COMM_WORLD);
  }

  if(argc != 2){
    printf("Require a argument.\n");
    exit(1);
  }

  sscanf(argv[1], "%d", &n);

  if((list_loads = (int *)malloc(n * sizeof(int))) == NULL){
    fprintf(stderr, "Memory allocation failed\n");
    exit(1);
  }
  printf("List loads : ");
  for(i=0; i<n; i++){
    list_loads[i] = rand()%(rand_max-rand_min+1)+rand_min;
    printf("%d, ", list_loads[i]);
  }
  printf("\n");

  method_balance = balance_one_swap;
  flag_verbose_balance = 1;
  err = balance_node(&send, &recv, list_loads, n, MPI_COMM_WORLD, MPI_COMM_WORLD, MPI_COMM_WORLD);

  free(list_loads);
  
  return 0;
}
