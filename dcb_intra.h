#ifndef _DCB_INTRA_H
#define _DCB_INTRA_H     1

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <unistd.h>

#include <mpi.h>
#include <omp.h>

#include <limits.h>
#include <math.h>

#include <sched.h>

// #include <libcpuid.h>
// #include <linux/getcpu.h>

#define ne_verbose           "DCB_VERBOSE" //Enabling verbose mode

#define ne_num_thrds_per_cpu "DCB_NUM_THREADS_PER_CPU" //Number of threads per CPU(Hardware context)
#define ne_num_cores         "DCB_NUM_CORES" //Using number of cores 
#define ne_num_cpus_per_core "DCB_NUM_CPUS_PER_CORE" //Using number of threads per core
#define ne_lcpu_excl         "DCB_LIST_CPUIDS_EXCLUDING" //Excluding list of Core ID for DCB
#define ne_lcore_excl        "DCB_LIST_COREIDS_EXCLUDING" //Excluding list of Core ID for DCB
#define ne_diffprocs_core    "DCB_DIFFERENT_PROCESS_ON_CORE" //Mapping different process on core when it using the hyper-threading 1 or 0
                                                             //If enabling this, it supports more balanced load. However, it may pollute the TLB.
#define ne_proc_diffsockets  "DCB_PROCESS_ON_DIFFERENT_SOCKETS" //Mapping a process on different sockets.
#define ne_bind_mode         "DCB_BIND_MODE" //node or socket :: Load-balancing in the node or socket. 
#define ne_max_num_presets   "DCB_MAX_NUM_PRESETS" //Maximum number of presets (Default is 100)
#define ne_mode_priority     "DCB_MODE_PRIORITY" //Mode of DCB "speed" denotes default mode, "power" denotes a mode which decreases number of cores per process for reducing power consumption, and "hybrid" denotes both speed and power.
#define ne_disable           "DCB_DISABLE" //Disabling DCB

#define ne_measuring_power   "DCB_MEASURING_POWER" //Measuring power
#define ne_output_params     "DCB_OUTPUT_PARAMS" //Outputting parameters passed to all processes

#define ne_minimum_cpus      "DCB_MINIMUM_CPUS" //Minimum number of cores 

#define MCW MPI_COMM_WORLD

struct info_cpu_t{
  int flag_alloc;
  int num_cpus, num_cores, num_sockets, max_cores_socket, logic_HT;
  int num_cpus_offline, num_cpus_online;
  int *num_cores_socket, *list_cpus_offline, *list_cpus_online;
  int **ptr_core_to_cpu, **ptr_socket_to_core, *core_to_cpu, *socket_to_core;
  int num_bound_cpus;
  int *mapped_list;
};

struct info_bind_t{
  int flag_alloc;
  int flag_diffprocs_core, flag_proc_diffsockets, bind_mode, mode_pr;
  int flag_disable;
  int flag_mes_power;
  int num_thrds_per_cpu, num_cpus_per_core;
  int num_cpus, num_cores;
  int num_excl_cpus, num_excl_cores;
  int cpuid_lowload;
  int *reserve_list, *excl_list_cpu, *excl_list_core;
  int last_valid_cpu;
  int *valid_cpu;
  int min_cpus;
};

struct preset_t{
  int flag;
  int ID, num_cpus, num_max_thrds, load;
  int *reserve_list;
};

// int flag_verbose, flag_enable_IO = 1, max_num_presets, flag_preset = 0;
extern int flag_verbose, flag_enable_IO, max_num_presets, flag_preset;
// struct info_cpu_t info_cpu = {.flag_alloc = 0};
// struct info_bind_t info_bind = {.flag_alloc = 0};
extern struct info_cpu_t info_cpu;
extern struct info_bind_t info_bind;
extern struct preset_t *presets;

extern MPI_Comm intra_comm, inter_comm, whole_comm;

#ifdef _measure_time
// int count_reserve = 0, count_bind = 0;
// double time_init = 0.0, time_reserve = 0.0, time_bind = 0.0, time_preset = 0.0;
extern int count_reserve, count_bind;
extern double time_init, time_reserve, time_bind, time_preset;
#endif

//whole_comm = communicator for all process
//intra_comm = communicator for processes which are in the same node
//inter_comm = communicator for processes among nodes (Processes which belongs to the communicator are master processes of intra_comm)

int get_cpuinfo(int **mapped_list, MPI_Comm const whole_comm, MPI_Comm const intra_comm);
int get_enval(MPI_Comm const whole_comm);

void set_zero_preset(struct preset_t *preset, int *mapped_list, MPI_Comm const comm_intra);

int create_intra_comm(MPI_Comm const whole_comm, MPI_Comm *intra_comm, MPI_Comm *inter_comm);
int separate_proc_socket(int **list_loads, int *init_socket_id, MPI_Comm const intra_comm, MPI_Comm *bound_comm);
int create_color_id(MPI_Comm const whole_comm);

int create_valid_table(MPI_Comm const intra_comm, MPI_Comm const inter_comm);

int balancing_core(int const load, MPI_Comm const intra_comm, MPI_Comm const inter_comm);
int reserve_cores(int const load, int *bcpus, int *num_max_thrds, int **reserve_list, MPI_Comm const intra_comm, MPI_Comm const inter_comm, MPI_Comm const whole_comm);
int reserve_cores_pa(int const load, int *bcpus, int *num_max_thrds, int **reserve_list, MPI_Comm const intra_comm, MPI_Comm const inter_comm, MPI_Comm const whole_comm);
int reserve_cores_mem(int const load, int *bcpus, int *num_max_thrds, int **reserve_list, MPI_Comm const intra_comm, MPI_Comm const inter_comm, MPI_Comm const whole_comm);
int bind_socket(int **list_loads, int *init_socket_id, MPI_Comm const intra_comm, MPI_Comm *bound_comm);
int bind_threads_to_cpu(int num_cpus, int num_max_thrds, int *reserve_list, MPI_Comm const intra_comm);

int verbose_bindinfo(char const *header, MPI_Comm const whole_comm);

#endif

