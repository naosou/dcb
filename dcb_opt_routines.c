#include "dcb_intra.h"
#include "libdcb.h"
#include "dcb_common.h"

#define len_line 8192
#define num_trials 4

#ifndef _path_powerstat
#define _path_powerstat "invalid"
#endif

int free_barrier, num_wait_procs, *list_wait_procs;
MPI_Comm save_intra_comm, *MPI_New_comm_ptr, intermid_comm;
int tag_barrier = 100;

int calc_digits(int const val);

int mpi_comm_f2c_f(MPI_Fint const comm_f, MPI_Comm *comm_C)
{
  // printf("Check comm_f2c bef %d\n", comm_f);
  *comm_C = MPI_Comm_f2c(comm_f);
  // printf("Check comm_f2c aft %d\n", comm_f);

  return dcb_success;
}

int mpi_comm_c2f_f(MPI_Comm *comm_C, MPI_Fint *comm_f)
{
  // printf("Check comm_c2f bef %d\n", *comm_f);
  *comm_f = MPI_Comm_c2f(*comm_C);
  // printf("Check comm_c2f aft %d\n", *comm_f);

  return dcb_success;
}

int dcb_start_power_measure(char const *header_fname, int const time_guess)
{
  int len_cmd;
  int me_proc_intra, me_proc_inter;
  char *cmd;
  char path[]=_path_powerstat;
  
  MPI_Comm_rank(intra_comm, &me_proc_intra);

  if(info_bind.flag_mes_power){
    if(flag_pmstat == 1){
      
      if(me_proc_intra == 0){
	printf("Check arguments, %s, %d\n", header_fname, time_guess);
	MPI_Comm_rank(inter_comm, &me_proc_inter);
	// len_cmd = sizeof(_path_powerstat);
	len_cmd = strlen(path) + 17 + calc_digits(time_guess) + 3 + strlen(header_fname) + 1 + 10 + 7;
	if((cmd = (char *)malloc(sizeof(char) * (len_cmd+1))) == NULL){ //Allocating array for command
	  fprintf(stderr, "DCB option : Memory allocation failed\n");
	  MPI_Abort(MCW, -1);
	}
	//Create command for powerstat
	sprintf(cmd, "%s/powerstat -RD 1 %d > %s_%i.txt &", _path_powerstat, time_guess, header_fname, me_proc_inter);
	printf("Check command %s\n", cmd);
	system(cmd);
	free(cmd);
      }
      MPI_Barrier(whole_comm);
    }else if(flag_papi){
#ifdef _enable_PowerAPI
      start_power_api(me_proc_intra);
#endif

    }
#ifdef _logging_clock

    int i, err, *mapped_list;
    pid_t pid;
    cpu_set_t mask_affinity;

    if((mapped_list = (int *)malloc(sizeof(int) * info_cpu.num_cpus)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(whole_comm, -1);
    }
    for(i=0; i<info_cpu.num_cpus; i++)
      mapped_list[i] = 0;

#pragma omp parallel default(none) shared(mapped_list, info_cpu) private(pid, err, i, mask_affinity)
    {
      pid = gettid();
      err = sched_getaffinity(pid, sizeof(mask_affinity), &mask_affinity); //Getting scheduled affinity
      for(i=0; i<info_cpu.num_cpus; i++){
	if(CPU_ISSET(i, &mask_affinity) != 0){
#pragma omp atomic
	  mapped_list[i]++;
	}
      }
    }
    
    /* for(i=0; i<info_cpu.num_cpus; i++) */
    /*   printf("%i, ", mapped_list[i]); */
    /* printf("\n"); */
    
    MPI_Allreduce(MPI_IN_PLACE, mapped_list, info_cpu.num_cpus, MPI_INT, MPI_SUM, intra_comm);

    if(me_proc_intra == 0){
      //Create command for powerstat
      len_cmd = 18 + calc_digits(time_guess) + 2*info_cpu.num_cpus + 3 + strlen(header_fname) + 1 + 10 + 7;
      if((cmd = (char *)malloc(sizeof(char) * (len_cmd+1))) == NULL){ //Allocating array for command
	fprintf(stderr, "DCB option : Memory allocation failed\n");
	MPI_Abort(MCW, -1);
      }
      sprintf(cmd, "./script_clock.sh %i", time_guess);
      for(i=0; i<info_cpu.num_cpus; i++)
	if(mapped_list[i] != 0)
	  sprintf(&(cmd[strnlen(cmd, len_cmd)]), " %i", i);
      sprintf(&(cmd[strnlen(cmd, len_cmd)]), " > %s_clock_%i.txt", header_fname, me_proc_inter);
      printf("Check command %s\n", cmd);
      // system(cmd);
    }
    
#endif
  }
  
  return dcb_success;
}

int dcb_stop_power_measure(char const *header_fname, int const time_guess, int const num_sockets, int const *pos_socket,
		      int const num_drms, int const *pos_drm)
{
  int i, j, l;
  int len_cmd;
  int flag_data, flag_fin, pos, pos_val;
  int me_proc_intra, me_proc_inter, num_procs_inter;
  float power, power_cpu, power_drm, power_node;
  float power_cpu_max, power_cpu_min, power_cpu_avg, power_cpu_sum;
  float power_drm_max, power_drm_min, power_drm_avg, power_drm_sum;
  float power_node_max, power_node_min, power_node_avg, power_node_sum;
  char *cmd, *cmd_start, *fname;
  char oneline[len_line], header[5];
  FILE *fp;
  char path[]=_path_powerstat;

  header[4] = '\0';

  if((flag_pmstat == 1 || flag_papi == 1) && info_bind.flag_mes_power){

    MPI_Comm_rank(intra_comm, &me_proc_intra);
    MPI_Comm_rank(inter_comm, &me_proc_inter);    
    MPI_Comm_size(inter_comm, &num_procs_inter);

    if(me_proc_intra == 0){
      if(flag_pmstat == 1){
	printf("Check arguments, %s, %d\n", header_fname, time_guess);
	// len_cmd = sizeof(_path_powerstat);
	len_cmd = strlen(path) + 17 + calc_digits(time_guess) + 1;
	if((cmd_start = (char *)malloc(sizeof(char) * (len_cmd+1))) == NULL){ //Allocating array for command
	  fprintf(stderr, "DCB option : Memory allocation failed\n");
	  MPI_Abort(MCW, -1);
	}
	sprintf(cmd_start, "%s/powerstat -RD 1 %d", _path_powerstat, time_guess); //Create command for powerstat

	len_cmd += 15 + 58;
	if((cmd = (char *)malloc(sizeof(char) * (len_cmd+1))) == NULL){ //Create command for killing powerstat command
	  fprintf(stderr, "DCB option : Memory allocation failed\n");
	  MPI_Abort(MCW, -1);
	}
      
	//Create command for killing process
	sprintf(cmd, "ps ax | grep \"%s\" | grep -v grep | tail -1 | awk '{print $1}' | xargs kill", cmd_start);
	printf("Check command %s\n", cmd);
	system(cmd);

	sleep(10);
	// fflush();

	if((fname = (char *)malloc(sizeof(char) * (strlen(header_fname)+1+10+4+1))) == NULL){ //Create command for killing powerstat command
	  fprintf(stderr, "DCB option : Memory allocation failed\n");
	  MPI_Abort(MCW, -1);
	}
	sprintf(fname, "%s_%i.txt", header_fname, me_proc_inter);
	printf("Check filename %s\n", fname);

	for(j=0; j<num_trials; j++){
	  fp = fopen(fname, "r");
	  if(fp == NULL){
	    fprintf(stderr, "powerstat command not found\n");
	    return -1;
	  }
	  
	  power_cpu = 0.0;
	  power_drm = 0.0;

	  flag_data = 0;
	  flag_fin = 0;
	  for(l=0; l<time_guess; l++){
	    fgets(oneline, len_line, fp); //Reading each line of result of powerstat command
	    if(fp == NULL) break;
	    // printf("Check each line : %s", oneline);
	    if(oneline[0] == '-'){
	      flag_fin = 1;
	      break;
	    }

	    for(pos=0; pos<len_line; pos++)
	      if(oneline[pos] != ' '){ //Skipping space
		break;
	      }
	    if(pos < len_line-4){	    
	      for(i=0;i<4;i++){
		header[i] = oneline[pos+i];
	      }
	      // printf("Read header %s fin\n", header);
	      if(!strcmp(header, "Time")){
		flag_data=1;  //If header of line is "Time", the next lines are result of power measuring
		// printf("Hit time, %s\n", header);
		continue;
	      }
	    }else{
	      // printf("Continue\n");
	      continue;
	    }
	  
	    if(flag_data)
	      {
		oneline[strlen(oneline)] = ' ';
		pos = 0;
		pos_val = -1;
		for(i=0;i<num_sockets;i++)
		  while(1){
		    if(oneline[pos] != ' '){  //Skipping space
		      pos_val += 1;
		      // printf("Not space pos=%d\n", pos_val);
		      if(pos_val == pos_socket[i]){
			sscanf(&(oneline[pos]), "%f", &power); //Reading power of each socket
			// printf("Reading cpu power=%f, id=%d\n", power, i);
			power_cpu += power;
		      }
		      while(1){
			if(oneline[pos] == ' ') break; //Skipping characters
			pos ++;
		      }
		      if(pos_val == pos_socket[i]) break;
		    }
		    pos++;
		  }
		pos = 0;
		pos_val = -1;
		for(i=0;i<num_drms;i++)
		  while(1){
		    if(oneline[pos] != ' '){  //Skipping space
		      pos_val += 1;
		      // printf("Not space pos=%d\n", pos_val);
		      if(pos_val == pos_drm[i]){
			sscanf(&(oneline[pos]), "%f", &power); //Reading power of each memory
			// printf("Reading drm power=%f, id=%d\n", power, i);
			power_drm += power;
		      }
		      while(1){
			if(oneline[pos] == ' ') break; //Skipping characters
			pos ++;
		      }
		      if(pos_val == pos_drm[i]) break;
		    }
		    pos++;
		  }
	      }
	  }
	  fclose(fp);

	  if(flag_fin){
	    break;
	  }else{
	    printf("Reload file open\n");
	  }
	}

	if(j == num_trials+1){
	  printf("Power measurement failed due to file update failure.\n");
	  return dcb_power_measuring_failure;
	}
	
	sprintf(cmd, "cp \"%s\" ./", fname);
	printf("Check command copy %s\n", cmd);
	system(cmd);
	sprintf(cmd, "rm \"%s\"", fname);
	printf("Check command rm %s\n", cmd);
	system(cmd);

      }else if(flag_papi == 1){
#ifdef _enable_PowerAPI
	stop_power_api(me_proc_intra);
	get_result_power_api(me_proc_intra, &power_cpu, &power_drm, &power_node);
#endif
      }

      //Taking statistics of results
      MPI_Reduce(&power_cpu, &power_cpu_sum, 1, MPI_FLOAT, MPI_SUM, 0, inter_comm);
      MPI_Reduce(&power_cpu, &power_cpu_min, 1, MPI_FLOAT, MPI_MIN, 0, inter_comm);
      MPI_Reduce(&power_cpu, &power_cpu_max, 1, MPI_FLOAT, MPI_MAX, 0, inter_comm);
      MPI_Reduce(&power_drm, &power_drm_sum, 1, MPI_FLOAT, MPI_SUM, 0, inter_comm);
      MPI_Reduce(&power_drm, &power_drm_min, 1, MPI_FLOAT, MPI_MIN, 0, inter_comm);
      MPI_Reduce(&power_drm, &power_drm_max, 1, MPI_FLOAT, MPI_MAX, 0, inter_comm);
      power_cpu_avg = power_cpu_sum / num_procs_inter;
      power_drm_avg = power_drm_sum / num_procs_inter;
      if(flag_papi == 1){
	MPI_Reduce(&power_node, &power_node_sum, 1, MPI_FLOAT, MPI_SUM, 0, inter_comm);
	MPI_Reduce(&power_node, &power_node_min, 1, MPI_FLOAT, MPI_MIN, 0, inter_comm);
	MPI_Reduce(&power_node, &power_node_max, 1, MPI_FLOAT, MPI_MAX, 0, inter_comm);
	power_node_avg = power_node_sum / num_procs_inter;
      }else if(flag_pmstat == 1){
	power_node_sum = power_cpu_sum+power_drm_sum;
	power_node_min = power_cpu_min+power_drm_min;
	power_node_max = power_cpu_max+power_drm_max;
	power_node_avg = power_cpu_avg+power_drm_avg;
      }
	
      
      if(me_proc_inter == 0){
	printf("Power measurement, cpu = %f, drm = %f, node = %f\n", power_cpu_sum, power_drm_sum, power_node_sum);
	printf("Power measurement_min, cpu = %f, drm = %f, node = %f\n", power_cpu_min, power_drm_min, power_node_min);
	printf("Power measurement_max, cpu = %f, drm = %f, node = %f\n", power_cpu_max, power_drm_max, power_node_max);
	printf("Power measurement_avg, cpu = %f, drm = %f, node = %f\n", power_cpu_avg, power_drm_avg, power_node_avg);
      }
    }
    MPI_Barrier(whole_comm);
  }
  return dcb_success;
}

int dcb_output_param(int const param, char const *fname, MPI_Comm const *comm_output)
{
  int nprocs, me_proc;
  int *all_params;
  FILE *fp;

  // printf("Check fcomm in output = %d\n", MPI_Fcomm);
  
  MPI_Comm_size(*comm_output, &nprocs);
  MPI_Comm_rank(*comm_output, &me_proc);
  
  if(me_proc == 0){
    if((all_params = (int *)malloc(sizeof(int) * nprocs)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(*comm_output, -1);
    }
  }
  
  MPI_Gather(&param, 1, MPI_INT, all_params, 1, MPI_INT, 0, *comm_output);

  if(me_proc == 0){
    if ((fp = fopen(fname, "w")) == NULL) {
      printf("DCB : File open failed.\n");
      printf("DCB : Aborting\n");
      exit(EXIT_FAILURE);
    }

    for(int i = 0; i < nprocs; i++)
      fprintf(fp, "%d, %d\n", i, all_params[i]);

    fclose(fp);
  }

  MPI_Barrier(*comm_output);
  
  return dcb_success;
}

//This function create new communicator without changing number of processes per node
int dcb_create_new_comm(char const *fname, MPI_Comm const *MPI_Old_comm, MPI_Comm *MPI_New_comm)
{
  int nprocs, me_proc;
  int *ranks_new, *ranks_map;
  FILE *fp;
  MPI_Group grp_old, grp_new;

  // printf("Check fcomm in newcomm = %d\n", MPI_Fcomm);

  // MPI_Barrier(MPI_COMM_WORLD);
  // printf("Inside create_new_comm\n");
  // MPI_Barrier(MPI_COMM_WORLD);

  // MPI_Barrier(*MPI_Old_comm);

  MPI_Comm_size(*MPI_Old_comm, &nprocs);
  MPI_Comm_rank(*MPI_Old_comm, &me_proc);
  
  if((ranks_new = (int *)malloc(sizeof(int) * nprocs)) == NULL){
    fprintf(stderr, "DCB : Memory allocation failed\n");
    MPI_Abort(*MPI_Old_comm, -1);
  }

  if(me_proc == 0){

    printf("fname for create new communicator = %s\n", fname);
    if ((fp = fopen(fname, "r")) == NULL) {
      printf("DCB : File open failed.\n");
      printf("DCB : Aborting\n");
      exit(EXIT_FAILURE);
    }

    int j;
    for(int i = 0; i < nprocs; i++){
      fscanf(fp, "%d %d\n", &j, &(ranks_new[i]));
      // printf("Reading data %d, %d\n", j, ranks_new[i]);
    }

    fclose(fp);
    // printf("Aft read %d\n", me_proc);

    //Check maximum proc_ID of array
    int proc;
    for(proc = 0; proc < nprocs; proc++)
      if(ranks_new[proc] == nprocs) break;
    
    //If maximum ranks_new == nprocs(Fortran style), decrement all ranks_new
    if(proc < nprocs) {
      for(int i = 0; i < nprocs; i++)
	ranks_new[i]--;
    }

    //Check all ranks are mapped
    if((ranks_map = (int *)calloc(nprocs, sizeof(int))) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(*MPI_Old_comm, -1);
    }
    for(int i = 0; i < nprocs; i++){
      proc = ranks_new[i];
      ranks_map[proc] = 1;
    }
    for(int i = 0; i < nprocs; i++){
      if(ranks_map[i] == 0){
	printf("There are unmapped process.\n");
	printf("Please check There are unmapped process.\n");
	MPI_Abort(*MPI_Old_comm, -1);
      }
    }
    free(ranks_map);
    
    // Output corresponding from old rank to new rank
    for(int i = 0; i < nprocs; i++)
      printf("%d ", i);
    printf("\n");
    for(int i = 0; i < nprocs; i++)
      printf("%d ", ranks_new[i]);
    printf("\n");
  }
  MPI_Bcast(ranks_new, nprocs, MPI_INT, 0, *MPI_Old_comm);
  
  /* for(int proc = 0; proc < nprocs; proc++){ */
  /*   if(me_proc == proc){ */
  /*     if(me_proc == 0) printf("\n"); */
  /*     printf("Check proc = %d, nprocs = %d\n", proc, nprocs); */
  /*     for(int i = 0; i < nprocs; i++) */
  /* 	printf("%d ", i); */
  /*     printf("\n"); */
  /*     for(int i = 0; i < nprocs; i++) */
  /* 	printf("%d ", ranks_new[i]); */
  /*     printf("\n"); */
  /*   } */
  /*   MPI_Barrier(MPI_COMM_WORLD); */
  /* } */
  /* if(me_proc == 0) printf("\n"); */
  
  MPI_Barrier(*MPI_Old_comm);
  // printf("Aft scatter %d\n", me_proc);
  
  if(MPI_Comm_group(*MPI_Old_comm, &grp_old) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_group\n");
    MPI_Abort(MCW, -1);
  }
  // printf("comm_group %d\n", me_proc);
  if(MPI_Group_incl(grp_old, nprocs, ranks_new, &grp_new) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_incl\n");
    MPI_Abort(MCW, -1);
  }
  // printf("group_incl %d\n", me_proc);
  if(MPI_Comm_create(*MPI_Old_comm, grp_new, MPI_New_comm) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_create\n");
    MPI_Abort(MCW, -1);
  }
  // printf("comm create %d\n", me_proc);

  return dcb_success;
}

//This function create new communicator that permits to change the number of processes per node.
// int dcb_create_new_comm_incproc(char const *fname, MPI_Comm const *MPI_Old_comm, MPI_Comm *MPI_New_comm, int argc, char *argv[])
int dcb_create_new_comm_incproc(char const *fname, MPI_Comm const *MPI_Old_comm, MPI_Comm *MPI_New_comm,
				int argc, char *strg_argv)
{
  int nprocs_whole, me_proc_whole, nprocs_intra, me_proc_intra, nprocs_inter, me_proc_inter;
  int *ranks_new, *ranks_map, *nodes_new, *ranks_new_local;
  int color, key;
  int high;
  int *num_procs_spawn;
  int err;
  int ptr, l_cname;
  int num_spawns_multiple, max_spawns;
  int num_procs_node;
  int *list_nodes;
  char **argv, ***argv_nodes;
  char **binaries;
  char nodename_temp[MPI_MAX_PROCESSOR_NAME], (*nodenames)[MPI_MAX_PROCESSOR_NAME] = NULL;
  MPI_Info *info_nodes;
  FILE *fp;
  MPI_Comm temp_inter_comm, temp_intra_comm, intercomm;
#ifdef __FUJITSU
  int sum_spawns;
  MPI_Info info_node_single;
#endif
  
  // printf("Check fcomm in newcomm = %d\n", MPI_Fcomm);

  // MPI_Barrier(MPI_COMM_WORLD);
  // printf("Inside create_new_comm\n");
  // MPI_Barrier(MPI_COMM_WORLD);

  // MPI_Barrier(*MPI_Old_comm);

  // printf("Check argc = %d\n", argc);
  // printf("Check base_argv = %s\n", strg_argv);

  argv  = (char **)malloc((argc+2)*sizeof(char *));
  ptr = 0;
  for(int i = 0; i < argc+1; i++){
    argv[i] = &(strg_argv[ptr]);
    while(1){
      if(strg_argv[ptr] == '\0'){
	ptr++;
	break;
      }
      ptr++;
    }	 
  }
  argv[argc+1] = NULL;
    
  /* for(int i = 0; i < argc+1; i++) */
  /*    printf("Check i = %d, argv = %s\n", i, argv[i]); */
  
  MPI_Comm_get_parent(&intercomm);
  if(intercomm == MPI_COMM_NULL){
    err = create_intra_comm(*MPI_Old_comm, &temp_intra_comm, &temp_inter_comm);
    
    MPI_Comm_size(*MPI_Old_comm, &nprocs_whole);
    MPI_Comm_rank(*MPI_Old_comm, &me_proc_whole);
    MPI_Comm_size(temp_intra_comm, &nprocs_intra);
    MPI_Comm_rank(temp_intra_comm, &me_proc_intra);
    MPI_Comm_size(temp_inter_comm, &nprocs_inter);
    MPI_Comm_rank(temp_inter_comm, &me_proc_inter);
    // printf("Check inside intercomm branch %d\n", me_proc_whole);
    
    if(me_proc_intra == 0) {
      if(me_proc_inter == 0){
	
	printf("DCB : fname for create new communicator with file = %s\n", fname);
	if ((fp = fopen(fname, "r")) == NULL) {
	  printf("DCB : File open failed.\n");
	  printf("DCB : Aborting\n");
	  exit(EXIT_FAILURE);
	}
	
	nprocs_whole = 0;
	char strg[100];
	while (fscanf(fp, "%[^\n] ", strg) != EOF){
	  // printf("Chekc read at fscanf = %s\n", strg);
	  nprocs_whole++;
	}
	fseek(fp,0,SEEK_SET);
	// printf("Check number of lines = %d\n", nprocs_whole);
	MPI_Bcast(&nprocs_whole, 1, MPI_INT, 0, temp_inter_comm); //Bcast1

	if((ranks_new = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){
	  fprintf(stderr, "DCB : Memory allocation failed\n");
	  MPI_Abort(*MPI_Old_comm, -1);
	}
	if((nodes_new = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){
	  fprintf(stderr, "DCB : Memory allocation failed\n");
	  MPI_Abort(*MPI_Old_comm, -1);
	}

	for(int i = 0; i < nprocs_whole; i++){
	  fscanf(fp, "%d %d\n", &(nodes_new[i]), &(ranks_new[i]));
	  // printf("Reading data %d, %d\n", nodes_new[i], ranks_new[i]);
	}
	
	fclose(fp);
	// printf("Aft read %d\n", me_proc);
	
	//Check maximum proc_ID of array
	int proc;
	for(proc = 0; proc < nprocs_whole; proc++)
	  if(ranks_new[proc] == nprocs_whole) break;
	
	//If maximum ranks_new == nprocs_whole(Fortran style), decrement all ranks_new
	if(proc < nprocs_whole) {
	  for(int i = 0; i < nprocs_whole; i++)
	    {
	      ranks_new[i]--;
	      nodes_new[i]--;
	    }
	}
	
	//Check all ranks are mapped
	if((ranks_map = (int *)calloc(nprocs_whole, sizeof(int))) == NULL){
	  fprintf(stderr, "DCB : Memory allocation failed\n");
	  MPI_Abort(*MPI_Old_comm, -1);
	}
	for(int i = 0; i < nprocs_whole; i++){
	  proc = ranks_new[i];
	  ranks_map[proc] = 1;
	}
	for(int i = 0; i < nprocs_whole; i++){
	  if(ranks_map[i] == 0){
	    printf("DCB : There are unmapped process.\n");
	    printf("DCB : Please check There are unmapped process.\n");
	    MPI_Abort(*MPI_Old_comm, -1);
	  }
	}
	free(ranks_map);
	
	// Output corresponding from old rank to new rank
	for(int i = 0; i < nprocs_whole; i++)
	  printf("%d ", i);
	printf("\n");
	for(int i = 0; i < nprocs_whole; i++)
	  printf("%d ", ranks_new[i]);
	printf("\n");
	fflush(stdout);
	
	MPI_Bcast(ranks_new, nprocs_whole, MPI_INT, 0, temp_inter_comm); //Bcast2
	MPI_Bcast(nodes_new, nprocs_whole, MPI_INT, 0, temp_inter_comm); //Bcast3
	
	num_procs_spawn = (int *)malloc(nprocs_inter*sizeof(int));
	for(int node = 0; node < nprocs_inter; node++)
	  num_procs_spawn[node] = -1;
	for(int proc = 0; proc < nprocs_whole; proc++)
	  num_procs_spawn[nodes_new[proc]]++;

	list_nodes = (int *)malloc(nprocs_inter*sizeof(int));
	num_spawns_multiple = 0;
	max_spawns = 0;
	for(int node = 0; node < nprocs_inter; node++)
	  if(num_procs_spawn[node] > 0){
	    num_procs_spawn[num_spawns_multiple] = num_procs_spawn[node];
	    list_nodes[num_spawns_multiple] = node;
	    num_spawns_multiple++;
	    if(max_spawns < num_procs_spawn[node]) max_spawns = num_procs_spawn[node];
	  }
	printf("Check num_spawns = %d, max_spawns_multiple = %d, nprocs_inter = %d\n", num_spawns_multiple, max_spawns, nprocs_inter);
#ifdef __FUJITSU
	sum_spawns = max_spawns * nprocs_inter;
#endif
	for(int spawn = 0; spawn < num_spawns_multiple; spawn++){
	  printf("DCB : Check cpawn, nodeID = %d, num spawns = %d\n", list_nodes[spawn], num_procs_spawn[spawn]);
	}
	fflush(stdout);

	nodenames = (char(*)[MPI_MAX_PROCESSOR_NAME])malloc(nprocs_inter*MPI_MAX_PROCESSOR_NAME*sizeof(char));
	MPI_Get_processor_name((char *)nodename_temp, &l_cname);
	MPI_Gather(nodename_temp, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, nodenames, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, temp_inter_comm); //Gather1

#ifdef __FUJITSU
	MPI_Info_create(&info_node_single);
	err = MPI_Info_set(info_node_single, "rank_map", "bynode");
	printf("Check1 err = %d, success = %d, me_proc = %d\n", err, MPI_SUCCESS, me_proc_whole);
	if(err != MPI_SUCCESS){
	  printf("DCB : Check return of MPI_Info_set, return = %d, ERR_INFO_KEY = %d, ERR_INFO_VALUE = %d, ERR_INTERN =%d\n",
		 err, MPI_ERR_INFO_KEY, MPI_ERR_INFO_VALUE, MPI_ERR_INTERN);
	}
#else
	info_nodes = (MPI_Info *)malloc(num_spawns_multiple*sizeof(MPI_Info));
	for(int spawn = 0; spawn < num_spawns_multiple; spawn++){
	  MPI_Info_create(&(info_nodes[spawn]));
	  err = MPI_Info_set(info_nodes[spawn], "host", nodenames[list_nodes[spawn]]);
	  printf("DCB : Check hostname of key, spawn = %d, hostname = %s\n", spawn, nodenames[list_nodes[spawn]]);
	  if(err != MPI_SUCCESS){
	    printf("DCB : Check return of MPI_Info_set, return = %d, ERR_INFO_KEY = %d, ERR_INFO_VALUE = %d, ERR_INTERN =%d\n",
		   err, MPI_ERR_INFO_KEY, MPI_ERR_INFO_VALUE, MPI_ERR_INTERN);
	  }
	}

	binaries = (char **)malloc(num_spawns_multiple*sizeof(char *));
	for(int spawn = 0; spawn < num_spawns_multiple; spawn++)
	  binaries[spawn] = argv[0];

	/* printf("Check binaries , "); */
	/* for(int spawn = 0; spawn < num_spawns_multiple; spawn++) */
	/*   printf("%d = %s, ", spawn, binaries[spawn]); */
	/* printf("\n"); */
	
	argv_nodes = (char ***)malloc(num_spawns_multiple*sizeof(char **));
	for(int spawn = 0; spawn < num_spawns_multiple; spawn++)
	  argv_nodes[spawn] = &argv[1];
	
	/* printf("Check argv, with spawn nodes, "); */
	/* for(int spawn = 0; spawn < num_spawns_multiple; spawn++){ */
	/*   printf("spawn = %d : ", spawn); */
	/*   for(int i = 0; i < argc; i++) */
	/*     printf("%d = %s, ", i, argv_nodes[spawn][i]); */
	/*   printf("\n"); */
	/* } */
	/* printf("\n"); */
#endif
      }else{
	MPI_Bcast(&nprocs_whole, 1, MPI_INT, 0, temp_inter_comm); //Bcast1
	// printf("Check recived nprocs_whole = %d\n", nprocs_whole);
	if((ranks_new = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){
	  fprintf(stderr, "DCB : Memory allocation failed\n");
	  MPI_Abort(*MPI_Old_comm, -1);
	}
	if((nodes_new = (int *)malloc(sizeof(int) * nprocs_whole)) == NULL){
	  fprintf(stderr, "DCB : Memory allocation failed\n");
	  MPI_Abort(*MPI_Old_comm, -1);
	}
	MPI_Bcast(ranks_new, nprocs_whole, MPI_INT, 0, temp_inter_comm); //Bcast2
	MPI_Bcast(nodes_new, nprocs_whole, MPI_INT, 0, temp_inter_comm); //Bcast3
	
	MPI_Get_processor_name((char *)nodename_temp, &l_cname);
	MPI_Gather(nodename_temp, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, nodenames, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, temp_inter_comm); //Gather1
      }

      num_procs_node = 0;
      for(int proc = 0; proc < nprocs_whole; proc++)
	if(nodes_new[proc] == me_proc_inter) num_procs_node++;
      
      if((ranks_new_local = (int *)malloc(sizeof(int) * num_procs_node)) == NULL){
	fprintf(stderr, "DCB : Memory allocation failed\n");
	MPI_Abort(*MPI_Old_comm, -1);
      }
      int ptr = 0;
      for(int proc = 0; proc < nprocs_whole; proc++)
	if(nodes_new[proc] == me_proc_inter){
	  ranks_new_local[ptr] = ranks_new[proc];
	  ptr++;
	}

      if(me_proc_inter == 0){
#ifdef __FUJITSU
	printf("DCB : Check spawn = %d\n", sum_spawns);
#else
	for(int spawn=0; spawn<num_spawns_multiple; spawn++)
	  printf("DCB : Check multiple spawn = %d, num_procs_spawn = %d, nodename = %s\n", 
		 spawn, num_procs_spawn[spawn], nodenames[list_nodes[spawn]]);
#endif
      }
    }
    fflush(stdout);
#ifdef __FUJITSU //Create vcoord file
    MPI_Comm_spawn(argv[0], argv[1], sum_spawns, info_node_single, 0, *MPI_Old_comm, &intercomm,
		   MPI_ERRCODES_IGNORE);
    // MPI_Comm_spawn(argv[0], argv[1], sum_spawns, MPI_INFO_NULL, 0, *MPI_Old_comm, &intercomm, MPI_ERRCODES_IGNORE);
#else
    MPI_Comm_spawn_multiple(num_spawns_multiple, binaries, argv_nodes, num_procs_spawn, info_nodes, 0, *MPI_Old_comm, &intercomm,
			    MPI_ERRCODES_IGNORE);
#endif
    high = 0;
    // MPI_Comm_free(&temp_intra_comm);
    // MPI_Comm_free(&temp_inter_comm);
  }else{
    high = 1;
  }
  MPI_Intercomm_merge(intercomm, high, &intermid_comm);
  
  err = create_intra_comm(intermid_comm, &save_intra_comm, &temp_inter_comm);
  
  MPI_Comm_size(intermid_comm, &nprocs_whole);
  MPI_Comm_rank(intermid_comm, &me_proc_whole);
  MPI_Comm_size(save_intra_comm, &nprocs_intra);
  MPI_Comm_rank(save_intra_comm, &me_proc_intra);
  MPI_Comm_size(temp_inter_comm, &nprocs_inter);
  MPI_Comm_rank(temp_inter_comm, &me_proc_inter);

  /* printf("Check comm whole, size = %d, rank =%d\n", nprocs_whole, me_proc_whole); */
  /* printf("Check comm intra, size = %d, rank =%d\n", nprocs_intra, me_proc_intra); */
  /* printf("Check comm inter, size = %d, rank =%d\n", nprocs_inter, me_proc_inter); */
  /* fflush(stdout); */

  MPI_Bcast(&num_procs_node, 1, MPI_INT, 0, save_intra_comm);
  // printf("Check node = %d, num_procs_node = %d\n", me_proc_whole, num_procs_node);
  if(me_proc_intra != 0){
    if((ranks_new_local = (int *)malloc(sizeof(int) * num_procs_node)) == NULL){
      fprintf(stderr, "DCB : Memory allocation failed\n");
      MPI_Abort(*MPI_Old_comm, -1);
    }
  }
  MPI_Bcast(ranks_new_local, num_procs_node, MPI_INT, 0, save_intra_comm);
  if(me_proc_intra < num_procs_node){
    color = 1;
    key = ranks_new_local[me_proc_intra];
  }else{
    color = 0; //Deleting process
    key = 0;
  }
   
  if(MPI_Comm_split(intermid_comm, color, key, MPI_New_comm) != MPI_SUCCESS){
    fprintf(stderr, "DCB : Failed MPI_Comm_split\n");
    MPI_Abort(MCW, -1);
  }
  MPI_New_comm_ptr = MPI_New_comm;

  err = dcb_init(intermid_comm);
  //Changing DCB_MODE to RC, instantly
  info_bind.mode_pr = 0;

  err = dcb_balance(color);

  err = dcb_free();
  // MPI_Comm_free(intermid_comm);

  // Restore DCB_MODE
  // info_bind.mode_pr = backup_dcb_mode;
  
  int sum_colors;
  MPI_Allreduce(&color, &sum_colors, 1, MPI_INT, MPI_SUM, save_intra_comm);
  printf("Check allreduce color = %d, sum_colors = %d\n", color, sum_colors);
  fflush(stdout);
  
  free_barrier = 0;
  if(sum_colors != nprocs_intra){ //Finishing unused process
    printf("Check with unused process, nproc_inter = %d\n", nprocs_inter);
    char expenv[100];
    sprintf(expenv, "DCB_NUM_CORES=%d", info_bind.num_cores-1);
    printf("Check exp1 : %s\n", expenv);
    // sprintf(expenv, "export DCB_NUM_CORES=%d", 4);
    putenv(expenv);
    // putenv("DCB_NUM_CORES=47");
    sprintf(expenv, "DCB_LIST_CPUIDS_EXCLUDING=%d", info_bind.last_valid_cpu);
    printf("Check exp2 : %s\n", expenv);
    putenv(expenv);
    // putenv("DCB_LIST_CPUIDS_EXCLUDING=0,1,59");
    // sprintf(expenv, "printenv > Check%d.txt", me_proc_whole);
    // system(expenv);

    int *list_color = (int *)malloc(nprocs_intra*sizeof(int));
    MPI_Gather(&color, 1, MPI_INT, list_color, 1, MPI_INT, 0, save_intra_comm);
    if(me_proc_intra == 0){
      free_barrier = 1;
      num_wait_procs = nprocs_intra - sum_colors;
      list_wait_procs = (int *)malloc(num_wait_procs * sizeof(int));
      ptr = 0;
      for(int proc = 0; proc < nprocs_intra; proc++){
	if(list_color[proc] == 0){
	  list_wait_procs[ptr] = proc;
	  ptr++;
	}		  
      }
      printf("Check list wait procs, num_wait_procs = %d, list = ", num_wait_procs);
      for(int proc = 0; proc < num_wait_procs; proc++)
	printf("%d, ", list_wait_procs[proc]);
      printf("\n");
      fflush(stdout);
    }
  }

  /* if(color == 0){ //Waiting unused process with receive */
  /*   printf("Finish process = %d\n", me_proc_whole); */
  /*   fflush(stdout); */
  /*   int temp; */
  /*   MPI_Recv(&temp, 1, MPI_INT, 0, tag_barrier, save_intra_comm, MPI_STATUS_IGNORE); */
  /*   MPI_Barrier(*MPI_Old_comm); */
  /*   // MPI_Comm_free(intermid_comm); */
  /*   // MPI_Comm_free(MPI_New_comm); */
  /*   MPI_Finalize(); */
  /*   exit(0); */
  /* } */

  return color;
}

void finalize_newcomm_incproc(){

  /* if(free_barrier){ */
  /*   int temp_send = 1; */
  /*   for(int proc=0; proc<num_wait_procs; proc++) */
  /*     MPI_Send(&temp_send, 1, MPI_INT, list_wait_procs[proc], tag_barrier, save_intra_comm); */
  /* } */
  // MPI_Comm_free(intermid_comm);
  // MPI_Comm_free(MPI_New_comm_ptr);

}

int calc_digits(int const val)
{
  int i;
  float fval;

  i = 0;
  fval = val;
  while(fval >= 1.0){
    i++;
    fval = fval / 10.0;
  }

  return i;
}
